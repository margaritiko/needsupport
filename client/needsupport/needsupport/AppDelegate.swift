//
//  AppDelegate.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Katana
import Tempura

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, RootInstaller {
    var window: UIWindow?
    var store: Store<AppState, DependenciesContainer>?

    func application(
        _: UIApplication,
        didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        store = Store<AppState, DependenciesContainer>(
            interceptors: [],
            stateInitializer: AppState.fakeState
        )

        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window

        let hasOpenedHome = UserDefaults.standard.bool(forKey: "HasOpenedHome")

        if hasOpenedHome {
            store?.dependencies?.navigator.start(
                using: self, in: window, at: Screen.homeScreen.rawValue
            )
        } else {
            store?.dependencies?.navigator.start(
                using: self, in: window, at: Screen.authorization.rawValue
            )
        }

        window.makeKeyAndVisible()

        return true
    }

    func installRoot(
        identifier: RouteElementIdentifier,
        context _: Any?,
        completion: @escaping Navigator.Completion
    ) -> Bool {
        guard let store = store else {
            return false
        }

        if identifier == Screen.homeScreen.rawValue {
            let viewController = HomeViewController(store: store)
            window?.rootViewController = viewController
            completion()
            return true
        } else if identifier == Screen.tasksCreation.rawValue {
            let viewController = TasksManagementViewController(
                store: store,
                localState: TasksManagementLocalState(type: .creation, selectedCategory: .none, mode: store.state.mode)
            )
            window?.rootViewController = viewController
            completion()
            return true
        } else if identifier == Screen.registration.rawValue {
            let viewController = ProfileViewController(
                store: store,
                localState: ProfileLocalState(type: .logIn, mode: store.state.mode)
            )
            window?.rootViewController = viewController
            completion()
            return true
        } else if identifier == Screen.authorization.rawValue {
            let viewController = AuthorizationViewController(store: store)
            window?.rootViewController = viewController
            completion()
            return true
        }

        return false
    }
}
