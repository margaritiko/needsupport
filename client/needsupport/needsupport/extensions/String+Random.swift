//
//  String+Random.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import Foundation

extension String {
    static func randomID(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0 ..< length).map { _ in letters.randomElement()! })
    }
}
