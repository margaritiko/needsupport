//
//  UIEdgeInsets.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import UIKit

extension UIEdgeInsets {
    init(common value: CGFloat) {
        self.init(top: value, left: value, bottom: value, right: value)
    }

    var horizontalSum: CGFloat {
        return left + right
    }

    var verticalSum: CGFloat {
        return top + bottom
    }
}
