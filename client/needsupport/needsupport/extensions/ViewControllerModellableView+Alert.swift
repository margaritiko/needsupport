//
//  ViewControllerModellableView+Alert.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import JSSAlertView
import Tempura

struct AlertConfiguration {
    let title: String
    let titleFont: UIFont
    let text: String
    let textFont: UIFont
    let buttonText: String
    let buttonFont: UIFont
    let backgroundColor: UIColor
    let textTheme: TextColorTheme
}

extension ViewControllerModellableView {
    func makeAlertConfiguration(title: String, text: String, buttonText: String, mode: AppState.Mode) -> AlertConfiguration {
        let brandBook = BrandBook.actual(mode)

        return AlertConfiguration(
            title: title,
            titleFont: FontManager.bold(ofSize: FontManager.h3FontSize),
            text: text,
            textFont: FontManager.regular(ofSize: FontManager.basicTextFontSize),
            buttonText: buttonText,
            buttonFont: FontManager.bold(ofSize: FontManager.h3FontSize),
            backgroundColor: brandBook.alertBackgroundColor,
            textTheme: mode == .light ? .light : .dark
        )
    }
}
