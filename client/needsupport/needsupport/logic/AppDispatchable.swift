//
//  AppDispatchable.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Katana

/// Syntactic sugar over StateUpdater and SideEffect

protocol AppStateUpdater: StateUpdater where StateType == AppState {}
protocol AppSideEffect: SideEffect where StateType == AppState, Dependencies == DependenciesContainer {}
