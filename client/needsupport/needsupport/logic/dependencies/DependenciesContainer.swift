//
//  DependenciesContainer.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Katana
import Tempura

final class DependenciesContainer: NavigationProvider {
    let dispatch: AnyDispatch

    var getAppState: () -> AppState

    var navigator: Navigator = .init()

    var getState: () -> State {
        return self.getAppState
    }

    required init(dispatch: @escaping AnyDispatch, getAppState: @escaping () -> AppState) {
        self.dispatch = dispatch
        self.getAppState = getAppState
    }
}

extension DependenciesContainer {
    convenience init(dispatch: @escaping AnyDispatch, getState: @escaping () -> State) {
        let getAppState: () -> AppState = {
            guard let state = getState() as? AppState else {
                fatalError("Wrong State Type")
            }
            return state
        }

        self.init(dispatch: dispatch, getAppState: getAppState)
    }
}
