//
//  AppNavigation.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Foundation
import Tempura

enum Screen: String {
    case registration
    case profile
    case tasksCreation
    case taskEdition
    case tasksReading
    case homeScreen
    case taskDetails
    case authorization
    case telegramAuthorization
    case taskEvents
}

extension HomeViewController: RoutableWithConfiguration {
    var routeIdentifier: RouteElementIdentifier {
        Screen.homeScreen.rawValue
    }

    var navigationConfiguration: [NavigationRequest: NavigationInstruction] {
        return [
            .show(Screen.taskDetails): .presentModally { [unowned self] task in
                guard let task = task as? AppState.Task else {
                    fatalError("Task should have type 'AppState.Task'")
                }
                let editingViewController = TaskDetailsViewController(
                    store: self.store,
                    localState: TasksDetailsLocalState(
                        type: .editing,
                        selectedTask: task,
                        mode: state.mode
                    )
                )
                editingViewController.modalPresentationStyle = .fullScreen
                return editingViewController
            },
            .show(Screen.tasksCreation): .presentModally { [unowned self] _ in
                let editingViewController = TasksManagementViewController(
                    store: self.store,
                    localState: TasksManagementLocalState(type: .creation, selectedCategory: nil, mode: state.mode)
                )
                return editingViewController
            },
            .show(Screen.tasksReading): .presentModally { [unowned self] task in
                guard let task = task as? AppState.Task else {
                    fatalError("Task should have type 'AppState.Task'")
                }
                let historyViewController = TaskDetailsViewController(
                    store: self.store,
                    localState: TasksDetailsLocalState(
                        type: .reading,
                        selectedTask: task,
                        mode: state.mode
                    )
                )
                return historyViewController
            },
            .show(Screen.profile): .presentModally { [unowned self] _ in
                let profileViewController = ProfileViewController(
                    store: self.store,
                    localState: ProfileLocalState(type: .crud(self.store.state.user), mode: state.mode)
                )
                return profileViewController
            },
        ]
    }
}

extension TasksManagementViewController: RoutableWithConfiguration {
    private var identifier: Screen {
        switch localState.type {
        case .creation:
            return Screen.taskEdition
        case .edition:
            return Screen.tasksCreation
        case .history:
            return Screen.tasksReading
        }
    }

    var routeIdentifier: RouteElementIdentifier {
        identifier.rawValue
    }

    var navigationConfiguration: [NavigationRequest: NavigationInstruction] {
        return [
            .hide(identifier): .dismissModally(behaviour: .hard),
        ]
    }
}

extension ProfileViewController: RoutableWithConfiguration {
    private var identifier: Screen {
        switch localState.type {
        case .logIn:
            return Screen.registration
        case .crud:
            return Screen.profile
        }
    }

    var routeIdentifier: RouteElementIdentifier {
        identifier.rawValue
    }

    var navigationConfiguration: [NavigationRequest: NavigationInstruction] {
        return [
            .hide(identifier): .dismissModally(behaviour: .hard),
            .show(Screen.homeScreen): .presentModally { [unowned self] _ in
                let homeViewController = HomeViewController(store: self.store)
                homeViewController.modalPresentationStyle = .fullScreen
                return homeViewController
            },
            .show(Screen.authorization): .presentModally { [unowned self] _ in
                let authorizationViewController = AuthorizationViewController(store: self.store)
                authorizationViewController.modalPresentationStyle = .fullScreen
                return authorizationViewController
            },
        ]
    }
}

extension TaskDetailsViewController: RoutableWithConfiguration {
    var routeIdentifier: RouteElementIdentifier {
        Screen.taskDetails.rawValue
    }

    var navigationConfiguration: [NavigationRequest: NavigationInstruction] {
        return [
            .hide(Screen.taskDetails): .dismissModally(behaviour: .hard),
            .show(Screen.taskEdition): .presentModally { [unowned self] task in
                guard let task = task as? AppState.Task else {
                    fatalError("Task should have type 'AppState.Task'")
                }
                let editingViewController = TasksManagementViewController(
                    store: self.store,
                    localState: TasksManagementLocalState(
                        type: .edition(task),
                        selectedCategory: task.category,
                        mode: state.mode
                    )
                )
                return editingViewController
            },
            .show(Screen.taskEvents): .presentModally { [unowned self] taskEvent in
                guard let taskEvents = taskEvent as? [AppState.TaskEvent] else {
                    fatalError("Task should have type 'AppState.TaskEvent'")
                }
                let historyViewController = TaskHistoryController(
                    store: self.store,
                    localState: TaskHistoryLocalState(taskEvents: taskEvents, mode: state.mode)
                )
                
                return historyViewController

            }
        ]
    }
}

extension AuthorizationViewController: RoutableWithConfiguration {
    var routeIdentifier: RouteElementIdentifier {
        Screen.authorization.rawValue
    }

    var navigationConfiguration: [NavigationRequest: NavigationInstruction] {
        return [
            .hide(Screen.authorization): .dismissModally(behaviour: .hard),
            .show(Screen.telegramAuthorization): .presentModally { [unowned self] _ in
                let webViewController = WebViewController(store: self.store)
                return webViewController
            },
        ]
    }
}

extension WebViewController: RoutableWithConfiguration {
    var routeIdentifier: RouteElementIdentifier {
        Screen.telegramAuthorization.rawValue
    }

    var navigationConfiguration: [NavigationRequest: NavigationInstruction] {
        return [
            .hide(Screen.telegramAuthorization): .dismissModally(behaviour: .hard),
            .show(Screen.homeScreen): .presentModally { [unowned self] _ in
                let homeViewController = HomeViewController(store: self.store)
                homeViewController.modalPresentationStyle = .fullScreen
                UserDefaults.standard.set(true, forKey: "HasOpenedHome")
                return homeViewController
            },
        ]
    }
}

extension TaskHistoryController: RoutableWithConfiguration {
    var routeIdentifier: RouteElementIdentifier {
        Screen.taskEvents.rawValue
    }

    var navigationConfiguration: [NavigationRequest : NavigationInstruction] {
        return [
            .hide(Screen.taskEvents): .dismissModally(behaviour: .hard),
        ]
    }
}
