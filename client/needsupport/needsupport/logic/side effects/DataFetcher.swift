//
//  DataUpdater.swift
//  needsupport
//
//  Created by Maria Manakhova on 21.03.2022.
//

import Foundation
import Katana

struct DataFetcher: SideEffect {
    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        fetchTasks(context: context)
        fetchUser(context: context)
    }
    
    private func fetchUser(context: SideEffectContext<AppState, DependenciesContainer>) {
        guard let url = URL(string: createUrl(path: "/api/users/\(HostSettings.userId)", queryItems: [])) else { return }
        
        var request = URLRequest(url: url)
        request.setValue(HostSettings.cookieHeaderValue, forHTTPHeaderField: "Cookie")
        request.setValue(HostSettings.acceptHeaderValue, forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            if let data = data {
                do {
                    guard
                        let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                        let user = json["data"] as? [String: Any]
                    else {
                        return
                    }
                    
                    let mapUser =
                    AppState.User(
                        ID: getIntValue(
                            model: user,
                            field: "id"
                        ),
                        firstName: getStringValue(
                            model: getMapValue(
                                model: user,
                                field: "info"
                            ),
                            field: "tg_first_name"
                        ),
                        lastName: getStringValue(
                            model: getMapValue(
                                model: user,
                                field: "info"
                            ),
                            field: "tg_last_name"
                        ),
                        telegramUserName: getStringValue(
                            model: getMapValue(
                                model: user,
                                field: "info"
                            ),
                            field: "tg_username"
                        ),
                        phoneNumber: getStringValue(
                            model: getMapValue(
                                model: user,
                                field: "info"
                            ),
                            field: "phone"
                        ),
                        pictureURLString: getStringValue(
                            model: getMapValue(
                                model: user,
                                field: "info"
                            ),
                            field: "tg_userpic"
                        )
                    )
                    
                    context.dispatch(UserStateUpdater(user: mapUser))
                    
                } catch {
                    print("Failed to fetch user data.")
                }
            } else if let error = error {
                print("Failed to fetch user data.")
            }
        }
        
        task.resume()
    }
    
    private func fetchTasks(context: SideEffectContext<AppState, DependenciesContainer>) {
        guard let url = URL(string: createUrl(
            path: "/api/tasks",
            queryItems: [("filter", "creator=\(HostSettings.userId)")]
        )
        ) else { return }
        
        var request = URLRequest(url: url)
        request.setValue(HostSettings.cookieHeaderValue, forHTTPHeaderField: "Cookie")
        request.setValue(HostSettings.acceptHeaderValue, forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            if let data = data {
                do {
                    guard
                        let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                        let tasks = json["data"] as? [[String: Any]]
                    else {
                        return
                    }
                    let mapTasks = tasks.map { task in
                        AppState.Task(
                            ID: getStringValue(
                                model: task,
                                field: "id"
                            ),
                            name: getStringValue(
                                model: getMapValue(
                                    model: task,
                                    field: "info"
                                ),
                                field: "name"
                            ),
                            description: getStringValue(
                                model: getMapValue(
                                    model: task,
                                    field: "info"
                                ),
                                field: "description"
                            ),
                            status: getTaskStatus(
                                status: getStringValue(
                                    model: getMapValue(
                                        model: getMapValue(
                                            model: task,
                                            field: "info"
                                        ),
                                        field: "statuses"
                                    ),
                                    field: "main"
                                ),
                                clarificationStatus: getBoolValue(
                                    model: getMapValue(
                                        model: getMapValue(
                                            model: task,
                                            field: "info"
                                        ),
                                        field: "statuses"
                                    ),
                                    field: "is_awaiting_client_clarifications"
                                )
                            ),
                            clientID: getIntValue(
                                model: task,
                                field: "creator"
                            ),
                            assigned: true,
                            category: .task
                        )
                    }
                    
                    context.dispatch(TasksUpdater(tasks: mapTasks))
                    
                } catch {
                    print("Failed to fetch tasks.")
                }
            } else if let error = error {
                print("Failed to fetch tasks.")
            }
        }
        
        task.resume()
    }
    
    private func getMapValue(model: [String: Any], field: String) -> [String: Any] {
        return (model[field] as? [String: Any])!
    }
    
//    private func getValue<T: Any>(model: [String: Any], field: String) -> T {
//        return model[field] as! T
//    }
//    
//    private func getMapValue(model: [String: Any], field: String) -> [String: Any] {
//        return (model[field] as? [String: Any])!
//    }
//    
    private func getStringValue(model: [String: Any], field: String) -> String {
        return model[field] as? String ?? ""
    }
    
    private func getIntValue(model: [String: Any], field: String) -> Int {
        return model[field] as! Int
    }
    
    private func getBoolValue(model: [String: Any], field: String) -> Bool {
        return (model[field] as? Bool) ?? false
    }
    
    private func getTaskStatus(status: String, clarificationStatus: Bool) -> AppState.TaskStatus {
        if clarificationStatus {
            return AppState.TaskStatus.awaitingClarification
        } else if status == "IN_WORK" {
            return AppState.TaskStatus.inProgress
        } else if status == "AWAITING_ASSIGNMENT" {
            return AppState.TaskStatus.awatingAssignment
        } else if status == "ASSIGNED" {
            return AppState.TaskStatus.assigned
        } else if status == "SOLVED" {
            return AppState.TaskStatus.completed
        }
        
        return AppState.TaskStatus.closed
    }
    
    private func createUrl(path: String, queryItems: [(name: String, value: String)]) -> String {
        var components = URLComponents()
        components.scheme = HostSettings.scheme
        components.host = HostSettings.host
        components.path = path
        var queryItemsArray: [URLQueryItem] = []
        for queryItem in queryItems {
            queryItemsArray.append(URLQueryItem(name: queryItem.name, value: queryItem.value))
        }
        if !queryItemsArray.isEmpty {
            components.queryItems = queryItemsArray
        }
        
        return components.string!
    }
}
