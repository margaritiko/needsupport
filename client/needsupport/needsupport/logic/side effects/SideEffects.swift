//
//  SideEffects.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Katana
import Tempura

struct ShowAddScreen: SideEffect {
    let task: AppState.Task?

    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        if task == nil {
            context.dispatch(Show(Screen.tasksCreation, animated: true))
        } else {
            context.dispatch(Show(Screen.taskDetails, animated: true, context: task))
        }
    }
}

struct ShowEditScreen: SideEffect {
    let task: AppState.Task?

    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        context.dispatch(Show(Screen.taskEdition, animated: true, context: task))
    }
}

struct ShowReadingScreen: SideEffect {
    let task: AppState.Task?

    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        context.dispatch(Show(Screen.tasksReading, animated: true, context: task))
    }
}

struct ShowTaskEventsScreen: SideEffect {
    let taskEvents: [AppState.TaskEvent]

    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        context.dispatch(Show(Screen.taskEvents, animated: true, context: taskEvents))
    }
}

struct ShowHomeScreen: SideEffect {
    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        context.dispatch(Show(Screen.homeScreen, animated: true))
    }
}

struct ShowProfileScreen: SideEffect {
    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        context.dispatch(Show(Screen.profile, animated: true))
    }
}

struct ShowRegistrationScreen: SideEffect {
    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        context.dispatch(Show(Screen.registration, animated: true))
    }
}

struct ShowTelegramAuthorizationScreen: SideEffect {
    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        context.dispatch(Show(Screen.telegramAuthorization, animated: true))
    }
}

struct ShowAuthorizationScreen: SideEffect {
    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        context.dispatch(Show(Screen.authorization, animated: true))
    }
}
