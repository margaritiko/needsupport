//
//  DataUpdater.swift
//  needsupport
//
//  Created by Maria Manakhova on 24.03.2022.
//

import Foundation
import Katana

struct TaskEventFetcher: SideEffect {
    
//    var taskID: String
    
    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        fetchTaskHistory(context: context)
    }
    
    private func fetchTaskHistory(context: SideEffectContext<AppState, DependenciesContainer>) {
        guard let url = URL(string: createUrl(
            path: "/api/task-events",
            queryItems: [
                ("page_size", "100"),
                ("filter", "creator=\(HostSettings.userId)")
            ]
        )
        ) else { return }
        
        var request = URLRequest(url: url)
        request.setValue(HostSettings.cookieHeaderValue, forHTTPHeaderField: "Cookie")
        request.setValue(HostSettings.acceptHeaderValue, forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            if let data = data {
                do {
                    guard
                        let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                        let taskEvents = json["data"] as? [[String: Any]]
                    else {
                        return
                    }
                    
                    let mapTaskEvents = taskEvents.map { taskEvent in
                        AppState.TaskEvent(
                            comment: getStringValue(
                                model: taskEvent,
                                field: "text"
                            ),
                            kind: getStringValue(
                                model: taskEvent,
                                field: "kind"
                            ),
                            userID: getIntValue(
                                model: taskEvent,
                                field: "creator"
                            ),
                            taskID: getStringValue(
                                model: taskEvent,
                                field: "task"
                            ),
                            type: getStringValue(
                                model: taskEvent,
                                field: "kind"
                            )
                        )
                    }
                    
                    context.dispatch(TasksEventUpdater(taskEvents: mapTaskEvents))
                    
                } catch {
                    print("Failed to fetch tasks.")
                }
            } else if let error = error {
                print("Failed to fetch tasks.")
            }
        }
        
        task.resume()
    }
    
    private func getMapValue(model: [String: Any], field: String) -> [String: Any] {
        return (model[field] as? [String: Any])!
    }
    
    private func getStringValue(model: [String: Any], field: String) -> String {
        return model[field] as? String ?? ""
    }
    
    private func getIntValue(model: [String: Any], field: String) -> Int {
        return model[field] as! Int
    }
    
    private func getBoolValue(model: [String: Any], field: String) -> Bool {
        return (model[field] as? Bool) ?? false
    }
    
    private func getTaskStatus(status: String, clarificationStatus: Bool) -> AppState.TaskStatus {
        if clarificationStatus {
            return AppState.TaskStatus.awaitingClarification
        } else if status == "IN_WORK" {
            return AppState.TaskStatus.inProgress
        } else if status == "AWAITING_ASSIGNMENT" {
            return AppState.TaskStatus.awatingAssignment
        } else if status == "ASSIGNED" {
            return AppState.TaskStatus.assigned
        } else if status == "SOLVED" {
            return AppState.TaskStatus.completed
        }
        
        return AppState.TaskStatus.closed
    }
    
    private func createUrl(path: String, queryItems: [(name: String, value: String)]) -> String {
        var components = URLComponents()
        components.scheme = HostSettings.scheme
        components.host = HostSettings.host
        components.path = path
        var queryItemsArray: [URLQueryItem] = []
        for queryItem in queryItems {
            queryItemsArray.append(URLQueryItem(name: queryItem.name, value: queryItem.value))
        }
        if !queryItemsArray.isEmpty {
            components.queryItems = queryItemsArray
        }
        
        return components.string!
    }
}
