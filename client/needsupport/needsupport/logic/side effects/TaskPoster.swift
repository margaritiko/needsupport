//
//  DataPoster.swift
//  needsupport
//
//  Created by Maria Manakhova on 22.03.2022.
//

import Foundation
import Katana

struct TaskPoster: SideEffect {
    let task: AppState.Task

    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        postTasks(context: context)
    }

    private func postTasks(context _: SideEffectContext<AppState, DependenciesContainer>) {
        guard let url = URL(string: createUrl(path: "/api/tasks")) else { return }

        var request = URLRequest(url: url)
        request.setValue(HostSettings.cookieHeaderValue, forHTTPHeaderField: "Cookie")
        request.setValue(HostSettings.acceptHeaderValue, forHTTPHeaderField: "Accept")
        request.setValue(HostSettings.acceptHeaderValue, forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"

        let parameters: [String: [String: Any]] = [
            "info": [
                "client": HostSettings.userId,
                "name": task.name,
                "description": task.description,
                "statuses": [
                    "main": "AWAITING_ASSIGNMENT"
                ]
            ],
        ]

        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                  let response = response as? HTTPURLResponse,
                  error == nil
            else { // check for fundamental networking error
                print("error", error ?? "Unknown error")
                return
            }

            guard (200 ... 299) ~= response.statusCode else { // check for http errors
                print("statusCode should be 2xx, but is \(response.statusCode)")
                print("response = \(response)")
                return
            }

            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(responseString)")
        }

        task.resume()
    }

    func createUrl(path: String) -> String {
        var components = URLComponents()
        components.scheme = HostSettings.scheme
        components.host = HostSettings.host
        components.path = path

        return components.string!
    }
}
