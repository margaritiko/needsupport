//
//  TaskUpdater.swift
//  needsupport
//
//  Created by Maria Manakhova on 22.03.2022.
//

import Foundation
import Katana

struct TaskUpdater: SideEffect {
    let task: AppState.Task
    
    func sideEffect(_ context: SideEffectContext<AppState, DependenciesContainer>) throws {
        postTasks(context: context)
    }
    
    private func postTasks(context _: SideEffectContext<AppState, DependenciesContainer>) {
        guard let url = URL(string: createUrl(path: "/api/tasks/\(task.ID)")) else { return }
        
        print(task.ID)
        print(task.comment)
        var request = URLRequest(url: url)
        request.setValue(HostSettings.cookieHeaderValue, forHTTPHeaderField: "Cookie")
        request.setValue(HostSettings.acceptHeaderValue, forHTTPHeaderField: "Accept")
        request.setValue(HostSettings.acceptHeaderValue, forHTTPHeaderField: "Content-Type")
        request.httpMethod = "PATCH"
        
        let parameters: [String: Any] = [
            "comment": task.comment,
            "data": [
                "info": [
                    "name": task.name,
                    "description": task.description,
                ],
            ],
        ]
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        
        let dataTask = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            if error != nil {
                print("error=\(error)")
                return
            }
            
            let responseString = NSString(data: data!, encoding:            String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
            //            guard let data = data,
            //                  let response = response as? HTTPURLResponse,
            //                  error == nil
            //            else { // check for fundamental networking error
            //                print("error", error ?? "Unknown error")
            //                return
            //            }
            //
            //            guard (200 ... 299) ~= response.statusCode else { // check for http errors
            //                print("statusCode should be 2xx, but is \(response.statusCode)")
            //                print("response = \(response.allHeaderFields)")
            //                return
            //            }
            //
            //            let responseString = String(data: data, encoding: .utf8)
            //            print("responseString = \(responseString)")
        }
        
        dataTask.resume()
    }
    
    func createUrl(path: String) -> String {
        var components = URLComponents()
        components.scheme = HostSettings.scheme
        components.host = HostSettings.host
        components.path = path
        
        return components.string!
    }
}
