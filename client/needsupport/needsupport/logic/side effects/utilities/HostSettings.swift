//
//  HostSettings.swift
//  needsupport
//
//  Created by Maria Manakhova on 21.03.2022.
//

struct HostSettings {
    @available(*, unavailable) private init() {}
    static let scheme: String = "https"
    static let oauthHost: String = "oauth.telegram.org"
    static let oauthPath: String = "/auth"
    static let host: String = "staging.crm.needsupport.ru"
    static let redirectLink: String = "https://staging.crm.needsupport.ru"
    static let botId: Int = 5_125_457_734
    static let cookieHeaderValue: String =  "session=.eJwlzkEOAjEIAMC_9OwBaKHgZwylEL3u6sn4d02cF8y73erI896uz-OVl3Z77HZtmzELTZRTbaiMMhydmKCyE6m64k7vhQ60ckWXqEKgiOkYEdsIVtZINgrbQiyBaLtcvaMNGIzLeSyYS5eSGE3PmhEUU639Iq8zj_-GgUEEJ7XPFzxxMQ4.YjnsEg.Nx8hoblVzij3qNrE0g8TWo24Awk"
    
//    "session=.eJwljjEOAyEMBP9CnQLb2MB95mSMraTlclWUvwcpU6402vmkM5Zfz3S81-2PdL5mOlImDZlWzDJVaCLgFafDqMTVCFGCOnkmB8mc2Y0UN-pDuRIyqomOQm7I4Tx8T9EFIUgsAwhstUqRrjoluEnoBK7Y9o9R2iH35etfA6V0aZg5fX_rEi_5.YjzOcQ.WFL7qMuM70kbWCSPO8V5Z0BM7QE"
    
    static let acceptHeaderValue: String = "application/json"
    
    static let userId: Int = 505066172
    // 57152196
    //
    //
}
