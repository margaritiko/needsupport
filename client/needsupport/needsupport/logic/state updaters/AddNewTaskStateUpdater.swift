//
//  AddNewTaskStateUpdater.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import Katana

struct AddNewTaskStateUpdater: AppStateUpdater {
    let task: AppState.Task

    func updateState(_ state: inout AppState) {
        state.tasks = [task] + state.tasks
    }
}
