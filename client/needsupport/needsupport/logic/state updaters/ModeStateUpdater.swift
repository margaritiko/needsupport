//
//  ModeStateUpdater.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Katana

struct LightModeStateUpdater: AppStateUpdater {
    func updateState(_ state: inout AppState) {
        state.mode = .light
    }
}

struct DarkModeStateUpdater: AppStateUpdater {
    func updateState(_ state: inout AppState) {
        state.mode = .dark
    }
}
