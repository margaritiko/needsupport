//
//  TaskEventUpdater.swift
//  needsupport
//
//  Created by Maria Manakhova on 25.03.2022.
//

import Foundation

struct TasksEventUpdater: AppStateUpdater {
    let taskEvents: [AppState.TaskEvent]

    func updateState(_ state: inout AppState) {
        state.taskEvents = taskEvents
    }
}
