//
//  TasksUpdater.swift
//  needsupport
//
//  Created by Maria Manakhova on 22.03.2022.
//

struct TasksUpdater: AppStateUpdater {
    let tasks: [AppState.Task]

    func updateState(_ state: inout AppState) {
        state.tasks = tasks
    }
}
