//
//  UpdateTaskStateUpdater.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import Katana

struct UpdateTaskStateUpdater: AppStateUpdater {
    let oldTask: AppState.Task

    let newTask: AppState.Task

    func updateState(_ state: inout AppState) {
        let index = state.tasks.firstIndex { $0.ID == oldTask.ID }
        guard let index = index else { return }
        state.tasks[index] = newTask
    }
}
