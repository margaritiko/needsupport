//
//  UserStateUpdater.swift
//  needsupport
//
//  Created by Rita Konnova on 05/03/2022.
//

import Katana

struct UserStateUpdater: AppStateUpdater {
    let user: AppState.User

    func updateState(_ state: inout AppState) {
        state.user = user
    }
}
