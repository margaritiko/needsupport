//
//  FontManager.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import UIKit

struct FontManager {
    // MARK: Constant Font Sizes

    static var h1FontSize = 24.0
    static var h2FontSize = 20.0
    static var h3FontSize = 16.0
    static var basicTextFontSize = 14.0

    // MARK: Custom Fonts `RFDewiExpanded`

    static func light(ofSize size: CGFloat) -> UIFont {
        guard
            let customFont = UIFont(name: "RFDewiExpanded-Light", size: size)
        else {
            fatalError("Failed to load the font")
        }

        return customFont
    }

    static func regular(ofSize size: CGFloat) -> UIFont {
        guard
            let customFont = UIFont(name: "RFDewiExpanded-Regular", size: size)
        else {
            fatalError("Failed to load the font")
        }

        return customFont
    }

    static func semibold(ofSize size: CGFloat) -> UIFont {
        guard
            let customFont = UIFont(name: "RFDewiExpanded-Semibold", size: size)
        else {
            fatalError("Failed to load the font")
        }

        return customFont
    }

    static func bold(ofSize size: CGFloat) -> UIFont {
        guard
            let customFont = UIFont(name: "RFDewiExtended-Bold", size: size)
        else {
            fatalError("Failed to load the font")
        }

        return customFont
    }

    static func ultrabold(ofSize size: CGFloat) -> UIFont {
        guard
            let customFont = UIFont(name: "RFDewiExpanded-Ultrabold", size: size)
        else {
            fatalError("Failed to load the font")
        }

        return customFont
    }
}
