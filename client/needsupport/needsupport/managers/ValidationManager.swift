//
//  ValidationManager.swift
//  needsupport
//
//  Created by Rita Konnova on 05/03/2022.
//

import Foundation

struct ValidationManager {
    enum Result {
        case error(String)
        case success
    }

    static func validateTaskTitle(_ title: String?) -> Result {
        guard let title = title, title.count > 0 else {
            return .error("Название задачи не может быть пустым")
        }
        if title.count < 3 {
            return .error("Название задачи слишком короткое")
        } else if title.count > 100 {
            return .error("Превышена допустимая длина названия задачи (100 символов)")
        }

        return .success
    }

    static func validateTaskDescription(_ description: String?) -> Result {
        guard let description = description, description.count > 0 else {
            return .error("Описание задачи не может быть пустым")
        }
        if description.count < 3 {
            return .error("Описание задачи слишком короткое")
        }

        return .success
    }

    static func validateName(_ name: String?) -> Result {
        guard let name = name, name.count > 0 else {
            return .error("Имя не введено")
        }

        return .success
    }

//    static func validateEmail(_ email: String?) -> Result {
//        guard let email = email, email.count > 0 else {
//            return .error("Не указана электронная почта")
//        }
//
//        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
//
//        let emailPred = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
//        if !emailPred.evaluate(with: email) {
//            return .error("Указана некорректная электронная почта")
//        }
//
//        return .success
//    }

    static func validatePhoneNumber(_ phone: String?) -> Result {
        guard let phone = phone, phone.count > 0 else {
            return .error("Не указан номер телефона")
        }

        let phoneRegEx = "^[0-9+]{0,1}+[0-9]{5,16}$"
        let phonePred = NSPredicate(format: "SELF MATCHES %@", phoneRegEx)
        if !phonePred.evaluate(with: phone) {
            return .error("Указан некорректный номер телефона")
        }

        return .success
    }
}
