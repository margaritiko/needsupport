//
//  AppState.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Foundation
import Katana

/// The definition of the main state of the app
struct AppState: State {
    enum Mode {
        case light
        case dark
    }

    var mode: Mode = .light

    var taskEvents: [TaskEvent] = []
    var user: User? = nil
    var tasks: [Task] = []
    var payments: [Payment] = []
    var messages: [Message] = []
    var supportRequests: [SupportRequest] = []
}

extension AppState {
    // MARK: TaskEvents

    struct TaskEvent {
        var comment: String
        var kind: String
        var userID: Int
        var taskID: String
        var type: String
    }

    // MARK: Tasks

    enum TaskStatus: CustomStringConvertible {
        case created
        case awatingAssignment
        case assigned
        case awaitingClarification
        case inProgress
        case completed
        case closed

        var description: String {
            switch self {
            case .created:
                return "Задача создана"
            case .inProgress:
                return "В работе"
            case .awatingAssignment:
                return "Ожидает назначения"
            case .assigned:
                return "Назначена"
            case .awaitingClarification:
                return "Ожидает пояснений"
            case .completed:
                return "Задача выполнена"
            case .closed:
                return "Выполнение прервано"
            }
        }

        var integerProgress: Int {
            switch self {
            case .created:
                return 10
            case .awatingAssignment:
                return 25
            case .assigned:
                return 50
            case .awaitingClarification:
                return 50
            case .inProgress:
                return 75
            case .completed:
                return 100
            case .closed:
                return 0
            }
        }
    }

    enum TaskCategory: CustomStringConvertible, CaseIterable, Equatable {
        case task

        static var allCases: [AppState.TaskCategory] = [.task]

        var description: String {
            return "Задача"
        }
    }

    struct Task: Equatable {
        var ID: String
        var name: String
        var description: String
        var status: TaskStatus
        var clientID: Int
        var assigned: Bool
        var category: TaskCategory
        var comment: String?
    }

    // MARK: Payment

    struct PaymentKind {
        var name: String
    }

    struct Payment {
        var ID: Int
        var kind: PaymentKind
        var totalAmount: Double
        var paymentURL: String
        var isAutopayment: Bool
        var creationDate: Date
        var processedDate: Date
        var isProcessed: Bool
    }

    // MARK: Users

    struct User {
        var ID: Int
        var firstName: String
        var lastName: String
        var telegramUserName: String
        var phoneNumber: String
        var pictureURLString: String
    }

    struct Message {
        var fromUserID: Int
        var text: String
        var isIncoming: Bool
        var content: String
        var creationDate: Date
        var processedDate: Date
        var isProcessed: Bool
    }

    struct SupportRequest {
        var userID: Int
        var isFeedbackPositive: Bool
        var creationDate: Date
        var endedDate: Date
    }
}

extension AppState {
    // Fake init for testing
    // TODO: Remove
    static func fakeState() -> AppState {
        self.init(
            mode: .light,
            taskEvents: [],
            //            user: User(ID: 0, name: "Роджер", phoneNumber: "+700000000", email: "lalala@gmail.com"),
            //            tasks: [
            //
            //                AppState.Task(
            //                    ID: "0", name: "Купить билеты в Лувр", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            //                    status: .created, clientID: 0, assigned: true, isPaid: true,
            //                    creationDate: Date(), completionDate: nil, category: .action
            //                ),
            //                AppState.Task(
            //                    ID: "1", name: "Написать сайт для фитнесс студии", description: "Описание",
            //                    status: .inProgress, clientID: 0, assigned: true, isPaid: true,
            //                    creationDate: Date(), completionDate: nil, category: .implementation
            //                ),
            //                AppState.Task(
            //                    ID: "2", name: "Сделать обзор примеров зон контроля транзита продовольственной продукции", description: "Описание",
            //                    status: .inProgress, clientID: 0, assigned: true, isPaid: true,
            //                    creationDate: Date(), completionDate: nil, category: .knowledge
            //                ),
            //                AppState.Task(
            //                    ID: "3", name: "Найти примеры создания цифровых решений для международной торговой и логистической инфраструктуры", description: "Описание",
            //                    status: .completed, clientID: 0, assigned: true, isPaid: true,
            //                    creationDate: Date(), completionDate: nil, category: .knowledge
            //                ),
            //                AppState.Task(
            //                    ID: "4", name: "Сделать подборку материалов по теме \"Метавселенная\"", description: "Описание",
            //                    status: .completed, clientID: 0, assigned: true, isPaid: true,
            //                    creationDate: Date(), completionDate: nil, category: .other("Составление подборки")
            //                ),
            //                AppState.Task(
            //                    ID: "5", name: "Найти информацию по использованию BIM в NASA", description: "Описание",
            //                    status: .closed, clientID: 0, assigned: true, isPaid: true,
            //                    creationDate: Date(), completionDate: nil, category: .knowledge
            //                ),
            //                AppState.Task(
            //                    ID: "6", name: "Найти примеры создания цифровых решений для международной торговой и логистической инфраструктуры", description: "Описание",
            //                    status: .completed, clientID: 0, assigned: true, isPaid: true,
            //                    creationDate: Date(), completionDate: nil, category: .knowledge
            //                ),
            //            ],
            payments: [],
            messages: [],
            supportRequests: []
        )
    }
}
