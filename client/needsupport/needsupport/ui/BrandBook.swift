//
//  BrandBook.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import UIKit

struct BrandBook {
    var backgroundColor: UIColor
    var firstTextColor: UIColor
    var secondTextColor: UIColor
    var thirdTextColor: UIColor
    var categoryTextColor: UIColor
    var timingsTextColor: UIColor
    var processHeaderTextColor: UIColor
    var processTextColor: UIColor
    var processBackgroundColor: UIColor
    var progressBarBackgroundColor: UIColor
    var progressBarForegroundColor: UIColor
    var inProcessBackgroundColor: UIColor
    var inProcessLabelBackgroundColor: UIColor
    var inProcessLabelForegroundColor: UIColor
    var completedTasksBackgroundColor: UIColor
    var completedLabelBackgroundColor: UIColor
    var completedLabelForegroundColor: UIColor
    var addTaskBackgroundColor: UIColor
    var enabledButtonBackgroundColor: UIColor
    var enabledButtonForegroundColor: UIColor
    var disabledButtonBackgroundColor: UIColor
    var disabledButtonForegroundColor: UIColor
    var alertBackgroundColor: UIColor
    var selectedChipBackgroundColor: UIColor
    var selectedChipForegroundColor: UIColor
    var notSelectedChipBackgroundColor: UIColor
    var notSelectedChipForegroundColor: UIColor
    var editTaskButtonBackgroundColor: UIColor
    var deleteTaskButtonBackgroundColor: UIColor
    var progressStatusTextColor: UIColor
    var imageBorderColor: UIColor
}

extension BrandBook {
    private static var light: BrandBook {
        BrandBook(
            backgroundColor: .white,
            firstTextColor: .black,
            secondTextColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 0.70),
            thirdTextColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 0.30),
            categoryTextColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 0.30),
            timingsTextColor: UIColor(red: 0.29, green: 0.10, blue: 0.85, alpha: 0.60),
            processHeaderTextColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.70),
            processTextColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.00),
            processBackgroundColor: UIColor(red: 0.29, green: 0.10, blue: 0.85, alpha: 1.00),
            progressBarBackgroundColor: UIColor(red: 0.77, green: 0.77, blue: 0.77, alpha: 1.00),
            progressBarForegroundColor: UIColor(red: 0.29, green: 0.10, blue: 0.85, alpha: 1.00),
            inProcessBackgroundColor: UIColor(red: 0.94, green: 0.94, blue: 0.98, alpha: 1.00),
            inProcessLabelBackgroundColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 0.40),
            inProcessLabelForegroundColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 1.00),
            completedTasksBackgroundColor: .white,
            completedLabelBackgroundColor: UIColor(red: 0.29, green: 0.10, blue: 0.85, alpha: 0.60),
            completedLabelForegroundColor: UIColor(red: 0.29, green: 0.10, blue: 0.85, alpha: 1.00),
            addTaskBackgroundColor: UIColor(red: 1.00, green: 0.77, blue: 0.72, alpha: 1.00),
            enabledButtonBackgroundColor: UIColor(red: 0.29, green: 0.10, blue: 0.85, alpha: 1.00),
            enabledButtonForegroundColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.70),
            disabledButtonBackgroundColor: UIColor(red: 0.70, green: 0.62, blue: 0.92, alpha: 1.00),
            disabledButtonForegroundColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.70),
            alertBackgroundColor: UIColor(red: 0.29, green: 0.10, blue: 0.85, alpha: 1.00),
            selectedChipBackgroundColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 1.00),
            selectedChipForegroundColor: .white,
            notSelectedChipBackgroundColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 0.40),
            notSelectedChipForegroundColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 1.00),
            editTaskButtonBackgroundColor: UIColor(red: 0.77, green: 0.77, blue: 0.77, alpha: 0.40),
            deleteTaskButtonBackgroundColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 0.40),
            progressStatusTextColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 1.00),
            imageBorderColor: UIColor(red: 0.70, green: 0.62, blue: 0.92, alpha: 1.00)
        )
    }

    private static var dark: BrandBook {
        BrandBook(
            backgroundColor: UIColor(red: 0.08, green: 0.05, blue: 0.16, alpha: 1.00),
            firstTextColor: UIColor(red: 0.90, green: 0.90, blue: 0.90, alpha: 0.80),
            secondTextColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.80),
            thirdTextColor: UIColor(red: 0.90, green: 0.90, blue: 0.90, alpha: 0.60),
            categoryTextColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.50),
            timingsTextColor: UIColor(red: 0.47, green: 0.43, blue: 0.96, alpha: 1.00),
            processHeaderTextColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.70),
            processTextColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.80),
            processBackgroundColor: UIColor(red: 0.29, green: 0.10, blue: 0.85, alpha: 1.00),
            progressBarBackgroundColor: UIColor(red: 0.77, green: 0.77, blue: 0.77, alpha: 1.00),
            progressBarForegroundColor: UIColor(red: 0.47, green: 0.28, blue: 1.00, alpha: 1.00),
            inProcessBackgroundColor: UIColor(red: 0.15, green: 0.04, blue: 0.46, alpha: 1.00),
            inProcessLabelBackgroundColor: UIColor(red: 0.87, green: 0.33, blue: 0.22, alpha: 1.00),
            inProcessLabelForegroundColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.60),
            completedTasksBackgroundColor: UIColor(red: 0.15, green: 0.04, blue: 0.46, alpha: 1.00),
            completedLabelBackgroundColor: UIColor(red: 0.29, green: 0.10, blue: 0.85, alpha: 1.00),
            completedLabelForegroundColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.60),
            addTaskBackgroundColor: UIColor(red: 0.87, green: 0.33, blue: 0.22, alpha: 1.00),
            enabledButtonBackgroundColor: UIColor(red: 0.29, green: 0.10, blue: 0.85, alpha: 1.00),
            enabledButtonForegroundColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.70),
            disabledButtonBackgroundColor: UIColor(red: 0.70, green: 0.62, blue: 0.92, alpha: 1.00),
            disabledButtonForegroundColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 0.70),
            alertBackgroundColor: UIColor(red: 0.70, green: 0.62, blue: 0.92, alpha: 1.00),
            selectedChipBackgroundColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 1.00),
            selectedChipForegroundColor: .white,
            notSelectedChipBackgroundColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 0.40),
            notSelectedChipForegroundColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 1.00),
            editTaskButtonBackgroundColor: UIColor(red: 0.77, green: 0.77, blue: 0.77, alpha: 0.40),
            deleteTaskButtonBackgroundColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 0.40),
            progressStatusTextColor: UIColor(red: 1.00, green: 0.48, blue: 0.36, alpha: 1.00),
            imageBorderColor: UIColor(red: 0.87, green: 0.33, blue: 0.22, alpha: 1.00)
        )
    }

    /// Returns the `BrandBook` with actual app colors
    static func actual(_ mode: AppState.Mode) -> BrandBook {
        switch mode {
        case .light:
            return .light
        case .dark:
            return .dark
        }
    }
}
