//
//  AuthorizationView.swift
//  needsupport
//
//  Created by Maria Manakhova on 20.03.2022.
//

import Foundation
import Tempura
import UIKit
import WebKit

class AuthorizationView: UIView, ViewControllerModellableView {
    private let backgroundImage = UIImageView(frame: .zero)
    let authButton = UIButton(frame: .zero)

    var didAuthTapButton: (() -> Void)?

    func setup() {
        addSubview(backgroundImage)
        addSubview(authButton)
        authButton.addTarget(self, action: #selector(authButtonTapped), for: .touchUpInside)
    }

    func style() {
        authButton.titleLabel?.font = FontManager.bold(ofSize: FontManager.h2FontSize)
        authButton.layer.masksToBounds = true
        authButton.setTitle(Specs.authButtonTitle, for: .normal)

        backgroundImage.contentMode = .scaleToFill
        backgroundImage.image = UIImage(named: "log-in-background")
    }

    func update(oldModel _: AuthorizationViewModel?) {
        guard let model = model else {
            return
        }

        // MARK: Colors Update

        let brandBook = BrandBook.actual(model.mode)
        backgroundColor = brandBook.backgroundColor
        authButton.backgroundColor = brandBook.enabledButtonBackgroundColor
        authButton.titleLabel?.textColor = brandBook.enabledButtonForegroundColor
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        backgroundImage.frame = CGRect(x: 0, y: frame.height / 5, width: frame.width, height: frame.height)
        authButton.pin
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .bottom(bounds.height / 2 - Specs.authButtonHeight / 2)
            .height(Specs.authButtonHeight)
        authButton.layer.cornerRadius = Specs.authButtonCornerRadius
    }

    @objc func authButtonTapped() {
        didAuthTapButton?()
    }
}

private enum Specs {
    static let safeAreaInsets = UIEdgeInsets(top: 70, left: 0, bottom: 0, right: 0)
    static let commonInsets = UIEdgeInsets(common: 32)
    static let authButtonTitle = "Авторизоваться"
    static let authButtonHeight = CGFloat(64.0)
    static let authButtonCornerRadius = CGFloat(12.0)
}
