//
//  AuthorizationController.swift
//  needsupport
//
//  Created by Maria Manakhova on 20.03.2022.
//

import JSSAlertView
import Tempura

class AuthorizationViewController: ViewController<AuthorizationView> {
    override func viewDidLoad() {
        super.viewDidLoad()
        updateMode()
    }

    override func setupInteraction() {
        rootView.didAuthTapButton = { [weak self] in
            self?.dispatch(ShowTelegramAuthorizationScreen())
        }
    }

    // MARK: UI

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        guard
            traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle
        else {
            return
        }

        updateMode()
    }

    private func updateMode() {
        switch traitCollection.userInterfaceStyle {
        case .dark:
            dispatch(DarkModeStateUpdater())
        case .light, .unspecified:
            dispatch(LightModeStateUpdater())
        @unknown default:
            fatalError()
        }
    }
}
