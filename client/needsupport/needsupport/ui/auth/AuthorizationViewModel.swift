//
//  AuthorizationViewModel.swift
//  needsupport
//
//  Created by Maria Manakhova on 20.03.2022.
//

import Tempura

struct AuthorizationViewModel: ViewModelWithState {
    let mode: AppState.Mode

    init(state: AppState) {
        mode = state.mode
    }
}
