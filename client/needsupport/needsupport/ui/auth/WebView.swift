//
//  WebView.swift
//  needsupport
//
//  Created by Maria Manakhova on 21.03.2022.
//

import Foundation
import Tempura
import UIKit
import WebKit

class WebView: UIView, ViewControllerModellableView {
    var webView: WKWebView!

    func setup() {
        webView = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
        webView.allowsBackForwardNavigationGestures = true
        webView.allowsLinkPreview = true

        addSubview(webView)
    }

    func load() {
        var components = URLComponents()

        components.scheme = HostSettings.scheme
        components.host = HostSettings.oauthHost
        components.path = HostSettings.oauthPath
        components.queryItems = [
            URLQueryItem(name: "bot_id", value: "\(HostSettings.botId)"),
            URLQueryItem(name: "origin", value: HostSettings.redirectLink),
            URLQueryItem(name: "embed", value: "1"),
            URLQueryItem(name: "request_access", value: "write"),
        ]

        let url = URL(string: components.string!)
        let request = URLRequest(url: url!)

        webView.load(request)
    }

    func style() {}

    func update(oldModel _: AuthorizationViewModel?) {}

    override func layoutSubviews() {
        super.layoutSubviews()

        webView.frame = bounds
    }
}
