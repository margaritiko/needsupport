//
//  WebViewController.swift
//  needsupport
//
//  Created by Maria Manakhova on 21.03.2022.
//

import JSSAlertView
import Tempura
import WebKit

class WebViewController: ViewController<WebView>, WKUIDelegate, WKNavigationDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()

        rootView.webView.uiDelegate = self
        rootView.webView.navigationDelegate = self

        rootView.load()
    }

    func webView(_: WKWebView,
                 didReceiveServerRedirectForProvisionalNavigation _: WKNavigation!)
    {
        dispatch(ShowHomeScreen())
    }
}
