//
//  HomeView.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import PinLayout
import Tempura
import UIKit

class HomeView: UIScrollView, ViewControllerModellableView {
    // MARK: Subviews

    private let greeting = UILabel(frame: .zero)
    private let openProfileButton = UIButton(frame: .zero)
    private let progressView = ProgressView()
    private let inProgressHeader = UILabel(frame: .zero)
    private let inProgressNumber = UILabel(frame: .zero)
    private let inProgressCollectionView = InProgressTasksListView(frame: .zero)
    private let historyHeader = UILabel(frame: .zero)
    private let historyNumber = UILabel(frame: .zero)
    private let historyCollectionView = HistoryTasksListView(frame: .zero)
    private let addTaskButton = UIButton(frame: .zero)

    var didTapAddTaskButton: (() -> Void)?
    var didTapProfileButton: (() -> Void)?
    var didTapInProgressTask: ((String) -> Void)? {
        didSet {
            inProgressCollectionView.didTapCell = didTapInProgressTask
        }
    }

    var didTapHistoryTask: ((String) -> Void)? {
        didSet {
            historyCollectionView.didTapCell = didTapHistoryTask
        }
    }

    // MARK: View Life Cycle

    func setup() {
        greeting.backgroundColor = .clear

        addSubview(greeting)
        addSubview(openProfileButton)
        addSubview(progressView)
        addSubview(inProgressHeader)
        addSubview(inProgressNumber)
        addSubview(inProgressCollectionView)
        addSubview(historyHeader)
        addSubview(historyNumber)
        addSubview(historyCollectionView)
        addSubview(addTaskButton)

        addTaskButton.addTarget(self, action: #selector(didTapAddButton), for: .touchUpInside)
        openProfileButton.addTarget(self, action: #selector(profileButtonTapped), for: .touchUpInside)

        // iPhones_5_5s_5c_SE or iPods
        if UIScreen.main.nativeBounds.height < 1334 {
            progressView.isHidden = true
        } else {
            progressView.isHidden = false
        }
    }

    func style() {
        // Top views

        greeting.font = FontManager.bold(ofSize: FontManager.h1FontSize)
        openProfileButton.backgroundColor = .clear
        openProfileButton.contentVerticalAlignment = .fill
        openProfileButton.contentHorizontalAlignment = .fill
        openProfileButton.imageEdgeInsets = Specs.buttonInsets

        // Progress view

        progressView.layer.cornerRadius = Specs.cardCornerRadius
        progressView.layer.masksToBounds = true

        // In progress section

        inProgressHeader.font = FontManager.bold(ofSize: FontManager.h1FontSize)
        inProgressHeader.text = Specs.inProgressHeaderText
        inProgressNumber.font = FontManager.bold(ofSize: FontManager.h3FontSize)
        inProgressNumber.textAlignment = .center
        inProgressNumber.layer.cornerRadius = Specs.labelCornerRadius
        inProgressNumber.layer.masksToBounds = true

        // History section

        historyHeader.font = FontManager.bold(ofSize: FontManager.h1FontSize)
        historyHeader.text = Specs.historyHeaderText
        historyNumber.font = FontManager.bold(ofSize: FontManager.h3FontSize)
        historyNumber.textAlignment = .center
        historyNumber.layer.cornerRadius = Specs.labelCornerRadius
        historyNumber.layer.masksToBounds = true

        // Add tasks button
        addTaskButton.contentVerticalAlignment = .fill
        addTaskButton.contentHorizontalAlignment = .fill
        addTaskButton.imageEdgeInsets = Specs.largerButtonInsets
        addTaskButton.layer.masksToBounds = true
    }

    func update(oldModel _: HomeVewModel?) {
        guard let model = model else {
            return
        }

        // MARK: Content Update

        greeting.text = model.greeting
        progressView.model = ProgressVewModel(homeViewModel: model)
        inProgressNumber.text = String(
            model.tasks.filter {
                $0.status == .inProgress ||
                    $0.status == .created ||
                    $0.status == .awatingAssignment ||
                    $0.status == .assigned ||
                    $0.status == .awaitingClarification
            }.count
        )
        historyNumber.text = String(
            model.tasks.filter { $0.status == .completed || $0.status == .closed }.count
        )
        inProgressCollectionView.model = ListViewModel(homeViewModel: model, type: .inProgress)
        historyCollectionView.model = ListViewModel(homeViewModel: model, type: .history)

        // MARK: Colors Update

        let brandBook = BrandBook.actual(model.mode)
        backgroundColor = brandBook.backgroundColor

        // Top views

        greeting.textColor = brandBook.firstTextColor

        let profileImage: UIImage?
        switch model.mode {
        case .light:
            profileImage = UIImage(named: "open-profile-light")
        case .dark:
            profileImage = UIImage(named: "open-profile-dark")
        }

        openProfileButton.setImage(profileImage, for: .normal)

        let addTaskImage: UIImage?
        switch model.mode {
        case .light:
            addTaskImage = UIImage(named: "add-task-light")
        case .dark:
            addTaskImage = UIImage(named: "add-task-dark")
        }

        addTaskButton.setImage(addTaskImage, for: .normal)

        // In progress section

        inProgressHeader.textColor = brandBook.firstTextColor
        inProgressNumber.backgroundColor = brandBook.inProcessLabelBackgroundColor
        inProgressNumber.textColor = brandBook.inProcessLabelForegroundColor

        // History section

        historyHeader.textColor = brandBook.firstTextColor
        historyNumber.backgroundColor = brandBook.completedLabelBackgroundColor
        historyNumber.textColor = brandBook.completedLabelForegroundColor

        addTaskButton.backgroundColor = brandBook.addTaskBackgroundColor

        // Additional: shadows

        progressView.layer.shadowColor = UIColor.black.cgColor
        progressView.layer.shadowOffset = CGSize(width: 0, height: 16.0)
        progressView.layer.shadowRadius = 16.0
        progressView.layer.shadowOpacity = 0.15
        progressView.layer.masksToBounds = false

        inProgressCollectionView.layer.shadowColor = UIColor.black.cgColor
        inProgressCollectionView.layer.shadowOffset = CGSize(width: 0, height: 16.0)
        inProgressCollectionView.layer.shadowRadius = 16.0
        inProgressCollectionView.layer.shadowOpacity = 0.05
        inProgressCollectionView.layer.masksToBounds = false

        historyCollectionView.layer.shadowColor = UIColor.black.cgColor
        historyCollectionView.layer.shadowOffset = CGSize(width: 0, height: 16.0)
        historyCollectionView.layer.shadowRadius = 16.0
        historyCollectionView.layer.shadowOpacity = 0.10
        historyCollectionView.layer.masksToBounds = false

        addTaskButton.layer.shadowColor = UIColor.black.cgColor
        addTaskButton.layer.shadowOffset = CGSize(width: 0, height: 16.0)
        addTaskButton.layer.shadowRadius = 16.0
        addTaskButton.layer.shadowOpacity = 0.20
        addTaskButton.layer.masksToBounds = false
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        // Top views

        openProfileButton.pin
            .top(Specs.safeAreaInsets.top)
            .right(Specs.commonInsets.right)
            .height(Specs.toolButtonSize)
            .width(Specs.toolButtonSize)

        greeting.pin
            .top(Specs.safeAreaInsets.top)
            .left(Specs.commonInsets.left)
            .width(frame.width - Specs.commonInsets.horizontalSum - Specs.toolButtonSize)
            .height(Specs.headerHeight)

        // Progress

        // iPhones_5_5s_5c_SE or iPods
        let progressViewHeight: CGFloat
        if UIScreen.main.nativeBounds.height < 1334 {
            progressViewHeight = 0
        } else {
            progressViewHeight = Specs.progressViewHeight
        }

        progressView.pin
            .below(of: greeting)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(progressViewHeight)

        // In progress section

        inProgressHeader.sizeToFit()
        let inProgressHederWidth = inProgressHeader.frame.width
        inProgressHeader.pin
            .below(of: progressView)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .width(inProgressHederWidth)
            .height(Specs.headerHeight)

        inProgressNumber.pin
            .below(of: progressView)
            .marginTop(Specs.smallMargin + inProgressHeader.frame.height / 4 + Specs.labelSize.height / 4)
            .after(of: inProgressHeader)
            .marginLeft(Specs.smallMargin)
            .size(Specs.labelSize)

        inProgressCollectionView.pin
            .below(of: inProgressHeader)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.inProgressCollectionViewHeight)

        // History section

        historyHeader.sizeToFit()
        let historyHeaderWidth = historyHeader.frame.width
        historyHeader.pin
            .below(of: inProgressCollectionView)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .width(historyHeaderWidth)
            .height(Specs.headerHeight)

        historyNumber.pin
            .below(of: inProgressCollectionView)
            .marginTop(Specs.smallMargin + historyHeader.frame.height / 4 + Specs.labelSize.height / 4)
            .after(of: historyHeader)
            .marginLeft(Specs.smallMargin)
            .size(Specs.labelSize)

        historyCollectionView.pin
            .below(of: historyHeader)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .bottom(Specs.commonInsets.bottom + Specs.scrollingBottomMargin)

        // Add task button

        addTaskButton.pin
            .bottom(Specs.commonInsets.bottom + Specs.scrollingBottomMargin * 1.5)
            .right(Specs.commonInsets.right)
            .size(Specs.toolButtonSize)

        addTaskButton.layer.cornerRadius = addTaskButton.frame.height / 2
    }

    // MARK: Interactions

    @objc func didTapAddButton() {
        didTapAddTaskButton?()
    }

    @objc func profileButtonTapped() {
        didTapProfileButton?()
    }
}

private enum Specs {
    static let scrollingBottomMargin = CGFloat(42.0)
    static let toolButtonSize = CGFloat(64.0)
    static let safeAreaInsets = UIEdgeInsets(top: 42, left: 0, bottom: 32, right: 0)
    static let commonInsets = UIEdgeInsets(common: 32)
    static let smallMargin = CGFloat(8)
    static let buttonInsets = UIEdgeInsets(common: 10)
    static let largerButtonInsets = UIEdgeInsets(common: 16)
    static var progressViewHeight: CGFloat {
        // iPhone_XR_11 and higher models have 1792
        if UIScreen.main.nativeBounds.height < 1792 {
            return CGFloat(100)
        } else {
            return CGFloat(120)
        }
    }

    static let cardCornerRadius = CGFloat(24)
    static let labelCornerRadius = CGFloat(4)
    static let headerHeight = CGFloat(64.0)
    static let inProgressHeaderText = "Выполняется"
    static let historyHeaderText = "История"
    static var inProgressCollectionViewHeight: CGFloat {
        // iPhone_XR_11 and higher models have 1792
        if UIScreen.main.nativeBounds.height < 1792 {
            return CGFloat(140)
        } else {
            return CGFloat(200)
        }
    }

    static let labelSize = CGSize(width: 36, height: 24)
}
