//
//  HomeViewController.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Tempura

class HomeViewController: ViewController<HomeView> {
    // MARK: Life Cycle

    let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        store.dispatch(DataFetcher())
        store.dispatch(TaskEventFetcher())
        updateMode()
        modalPresentationStyle = .fullScreen
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        rootView.refreshControl = refreshControl
    }

    @objc func refresh() {
        refreshControl.endRefreshing()
        self.store.dispatch(DataFetcher())

    }

    // MARK: Logic

    override func setupInteraction() {
        rootView.didTapAddTaskButton = {
            let stateUpdater = ShowAddScreen(task: nil)
            self.store.dispatch(stateUpdater)
        }

        rootView.didTapInProgressTask = { identifier in
            let stateUpdater = ShowAddScreen(task: self.state.tasks.first { $0.ID == identifier })
            self.store.dispatch(stateUpdater)
        }

        rootView.didTapHistoryTask = { identifier in
            let stateUpdater = ShowReadingScreen(task: self.state.tasks.first { $0.ID == identifier })
            self.store.dispatch(stateUpdater)
        }

        rootView.didTapProfileButton = {
            self.store.dispatch(ShowProfileScreen())
        }
    }

    // MARK: UI

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        guard
            traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle
        else {
            return
        }

        updateMode()
    }

    private func updateMode() {
        switch traitCollection.userInterfaceStyle {
        case .dark:
            dispatch(DarkModeStateUpdater())
        case .light, .unspecified:
            dispatch(LightModeStateUpdater())
        @unknown default:
            fatalError()
        }
    }
}
