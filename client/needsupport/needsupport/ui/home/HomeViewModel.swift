//
//  HomeViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Tempura

struct HomeVewModel: ViewModelWithState {
    let userName: String

    let mode: AppState.Mode

    let tasks: [AppState.Task]

    var greeting: String {
        if UIScreen.main.nativeBounds.height < 1334 {
            return "Привет!"
        } else {
            return "Привет, \(userName)!"
        }
    }

    init(state: AppState) {
        userName = state.user?.firstName ?? "..."
        tasks = state.tasks
        mode = state.mode
    }
}
