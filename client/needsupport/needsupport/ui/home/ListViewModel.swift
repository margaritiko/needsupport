//
//  ListViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Tempura

struct ListViewModel: ViewModel {
    enum TaskType {
        case inProgress
        case history
    }

    let mode: AppState.Mode
    let tasks: [AppState.Task]

    init(homeViewModel: HomeVewModel, type: TaskType) {
        switch type {
        case .inProgress:
            tasks = homeViewModel.tasks.filter {
                $0.status == .created ||
                    $0.status == .awatingAssignment ||
                    $0.status == .assigned ||
                    $0.status == .awaitingClarification ||
                    $0.status == .inProgress
            }
        case .history:
            tasks = homeViewModel.tasks.filter {
                $0.status == .completed || $0.status == .closed
            }
        }
        mode = homeViewModel.mode
    }
}
