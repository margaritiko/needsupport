//
//  HistoryTaskCell.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Tempura
import UIKit

class HistoryTaskCell: UICollectionViewCell, ModellableView {
    private let category = UILabel(frame: .zero)
    private let title = UILabel(frame: .zero)
    private let status = UILabel(frame: .zero)
//    private let duration = UILabel(frame: .zero)

    override init(frame _: CGRect) {
        super.init(frame: .zero)

        setup()
        style()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: `ModellableView`

    func setup() {
        addSubview(category)
        addSubview(title)
        addSubview(status)
//        addSubview(duration)

        // iPhone_XR_11 and higher models have 1792
        if UIScreen.main.nativeBounds.height < 1792 {
            category.isHidden = true
        } else {
            category.isHidden = false
        }
    }

    func style() {
        category.font = FontManager.regular(ofSize: FontManager.basicTextFontSize)
        title.font = FontManager.bold(ofSize: FontManager.h3FontSize)
        status.font = FontManager.regular(ofSize: FontManager.basicTextFontSize)
//        duration.font = FontManager.regular(ofSize: FontManager.basicTextFontSize)
        title.numberOfLines = 0
        layer.cornerRadius = Specs.cardCornerRadius
        layer.masksToBounds = true
    }

    func update(oldModel _: HistoryTaskViewModel?) {
        guard let model = model else { return }

        // Content

        category.text = model.task.category.description
        title.text = model.task.name
        status.text = model.task.status.description.lowercased()
//        duration.text = {
//            if model.duration % 10 == 1 {
//                return "1 день"
//            } else if model.duration % 10 >= 2, model.duration % 10 <= 4 {
//                return "\(model.duration) дня"
//            } else {
//                return "\(model.duration) дней"
//            }
//        }()

        // Colors

        let brandBook = BrandBook.actual(model.mode)
        category.textColor = brandBook.categoryTextColor
        title.textColor = brandBook.secondTextColor
        status.textColor = brandBook.timingsTextColor
//        duration.textColor = brandBook.timingsTextColor
        backgroundColor = brandBook.completedTasksBackgroundColor
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        category.pin
            .top(Specs.commonInsets.top)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.smallTextInsets.verticalSum + FontManager.basicTextFontSize)

        status.pin
            .bottom(Specs.commonInsets.bottom)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.smallTextInsets.verticalSum + FontManager.basicTextFontSize)

//        duration.pin
//            .bottom(Specs.commonInsets.bottom)
//            .left(Specs.commonInsets.left)
//            .right(Specs.commonInsets.right)
//            .height(Specs.smallTextInsets.verticalSum + FontManager.basicTextFontSize)

        // iPhone_XR_11 and higher models have 1792
        if UIScreen.main.nativeBounds.height < 1792 {
            title.pin
                .top(Specs.commonInsets.top)
                .above(of: status)
//                .above(of: duration)
                .marginBottom(Specs.commonMargin * 2)
                .left(Specs.commonInsets.left)
                .right(Specs.commonInsets.right)
        } else {
            title.pin
                .below(of: category)
                .marginTop(Specs.commonMargin)
                .above(of: status)
//                .above(of: duration)
                .marginBottom(Specs.commonMargin * 2)
                .left(Specs.commonInsets.left)
                .right(Specs.commonInsets.right)
        }
    }
}

private enum Specs {
    static let commonInsets = UIEdgeInsets(top: 8, left: 16, bottom: 16, right: 16)
    static let commonMargin = CGFloat(4.0)
    static let smallTextInsets = UIEdgeInsets(common: 2)
    static let cardCornerRadius = CGFloat(16.0)
}
