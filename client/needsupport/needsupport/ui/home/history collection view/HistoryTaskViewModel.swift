//
//  HistoryTaskViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Tempura

struct HistoryTaskViewModel: ViewModel {
    let task: AppState.Task

    let status: String

    let mode: AppState.Mode

    init(task: AppState.Task, mode: AppState.Mode) {
        self.task = task
        self.mode = mode
        status = task.status.description.lowercased()
//        duration = 2 // TODO: Support dates [task.completionDate - task.creationDate]
    }
}
