//
//  HistoryTasksListView.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Tempura
import UIKit

class HistoryTasksListView: UIView, ModellableView {
    private var collectionView: UICollectionView

    var didTapCell: ((String) -> Void)?

    override init(frame: CGRect) {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)

        super.init(frame: frame)

        setup()
        style()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(
            HistoryTaskCell.self,
            forCellWithReuseIdentifier: Constants.credentialsCellReuseIdentifier
        )

        addSubview(collectionView)
    }

    func style() {
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = .clear
    }

    func update(oldModel _: ListViewModel?) {
        DispatchQueue.main.async {
            self.layoutIfNeeded()
            self.collectionView.reloadData()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        collectionView.pin.all()
    }
}

extension HistoryTasksListView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(
        _: UICollectionView,
        numberOfItemsInSection _: Int
    ) -> Int {
        return model?.tasks.count ?? 0
    }

    func collectionView(
        _: UICollectionView,
        layout _: UICollectionViewLayout,
        sizeForItemAt _: IndexPath
    ) -> CGSize {
        return CGSize(
            width: frame.width - Specs.commonInsets.horizontalSum,
            height: frame.height * 3 / 4 - Specs.commonInsets.verticalSum
        )
    }

    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        guard
            let model = model,
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: Constants.credentialsCellReuseIdentifier,
                for: indexPath
            ) as? HistoryTaskCell
        else {
            fatalError("Wrong class for cell")
        }

        let tasks = model.tasks
        guard tasks.count >= indexPath.row else {
            fatalError("No task was found at index \(indexPath.row)")
        }

        cell.model = HistoryTaskViewModel(task: model.tasks[indexPath.row], mode: model.mode)

        return cell
    }

    func collectionView(_: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let model = model else { return }

        let tasks = model.tasks
        guard tasks.count >= indexPath.row else {
            fatalError("No task was found at index \(indexPath.row)")
        }

        didTapCell?(model.tasks[indexPath.row].ID)
    }
}

private enum Constants {
    static let credentialsCellReuseIdentifier = "HistoryTasksCell"
}

private enum Specs {
    static let commonInsets = UIEdgeInsets(common: 4)
}
