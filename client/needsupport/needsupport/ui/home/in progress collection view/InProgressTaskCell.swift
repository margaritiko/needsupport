//
//  InProgressTaskCell.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Tempura
import UIKit

class InProgressTaskCell: UICollectionViewCell, ModellableView {
    private let category = UILabel(frame: .zero)
    private let title = UILabel(frame: .zero)
    private let progressBarBody = UIView(frame: .zero)
    private let progressBar = UIView(frame: .zero)

    override init(frame _: CGRect) {
        super.init(frame: .zero)

        setup()
        style()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: `ModellableView`

    func setup() {
        addSubview(category)
        addSubview(title)
        addSubview(progressBarBody)
        addSubview(progressBar)
    }

    func style() {
        category.font = FontManager.regular(ofSize: FontManager.basicTextFontSize)
        title.font = FontManager.bold(ofSize: FontManager.h3FontSize)
        title.numberOfLines = 0
        layer.cornerRadius = Specs.cardCornerRadius
        layer.masksToBounds = true
        progressBar.layer.masksToBounds = true
        progressBarBody.layer.masksToBounds = true
    }

    func update(oldModel _: InProgressTaskViewModel?) {
        guard let model = model else { return }

        // Content

        category.text = model.task.category.description
        title.text = model.task.name

        // Colors

        let brandBook = BrandBook.actual(model.mode)
        category.textColor = brandBook.categoryTextColor
        title.textColor = brandBook.secondTextColor
        progressBarBody.backgroundColor = brandBook.progressBarBackgroundColor
        progressBar.backgroundColor = brandBook.progressBarForegroundColor
        backgroundColor = brandBook.inProcessBackgroundColor
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        guard let model = model else { return }

        category.pin
            .top(Specs.commonInsets.top)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.smallTextInsets.verticalSum + FontManager.basicTextFontSize)

        progressBarBody.pin
            .bottom(Specs.commonInsets.bottom)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.progressBarHeight)

        progressBar.pin
            .bottom(Specs.commonInsets.bottom)
            .left(Specs.commonInsets.left)
            .width(progressBarBody.frame.width * model.progress)
            .height(Specs.progressBarHeight)

        title.pin
            .below(of: category)
            .marginTop(Specs.commonMargin)
            .above(of: progressBar)
            .marginBottom(Specs.commonMargin * 2)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)

        progressBarBody.layer.cornerRadius = progressBarBody.frame.height / 2
        progressBar.layer.cornerRadius = progressBar.frame.height / 2
    }
}

private enum Specs {
    static let commonInsets = UIEdgeInsets(top: 8, left: 16, bottom: 16, right: 16)
    static let commonMargin = CGFloat(4.0)
    static let smallTextInsets = UIEdgeInsets(common: 2)
    static let progressBarHeight = CGFloat(8.0)
    static let cardCornerRadius = CGFloat(16.0)
}
