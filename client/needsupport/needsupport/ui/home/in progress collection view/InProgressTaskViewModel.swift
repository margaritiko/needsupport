//
//  InProgressTaskViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Tempura

struct InProgressTaskViewModel: ViewModel {
    let task: AppState.Task

    let progress: CGFloat

    let mode: AppState.Mode

    init(task: AppState.Task, mode: AppState.Mode) {
        self.task = task
        self.mode = mode
        progress = CGFloat(self.task.status.integerProgress) / 100
    }
}
