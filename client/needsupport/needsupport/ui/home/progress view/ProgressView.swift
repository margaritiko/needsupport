//
//  ProgressView.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import PinLayout
import Tempura
import UIKit

class ProgressView: UIView, ModellableView {
    // MARK: Subviews

    private let headerLabel = UILabel(frame: .zero)
    private let progressLabel = UILabel(frame: .zero)
    private let backgroundImage = UIImageView(frame: .zero)

    // MARK: View Life Cycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        style()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        addSubview(backgroundImage)
        addSubview(headerLabel)
        addSubview(progressLabel)

        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:))))
    }

    @objc func handleTap(sender _: UITapGestureRecognizer) {
        UIView.animate(
            withDuration: 0.15,
            delay: 0,
            usingSpringWithDamping: 0.9,
            initialSpringVelocity: 3,
            options: [.curveEaseIn],
            animations: {
                self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            },
            completion: { _ in
                UIView.animate(
                    withDuration: 0.15,
                    delay: 0,
                    usingSpringWithDamping: 0.5,
                    initialSpringVelocity: 3,
                    options: [.curveEaseInOut]
                ) {
                    self.transform = CGAffineTransform.identity
                }
            }
        )
    }

    func style() {
        headerLabel.font = FontManager.bold(ofSize: FontManager.h3FontSize)
        progressLabel.font = FontManager.bold(ofSize: FontManager.h1FontSize)
        headerLabel.text = Specs.headerText
        backgroundImage.image = UIImage(named: "hand")
        backgroundImage.contentMode = .scaleAspectFit
    }

    func update(oldModel _: ProgressVewModel?) {
        guard let model = model else {
            return
        }

        // Content
        progressLabel.text = Specs.progressText(
            done: model.completed, total: model.total
        )

        // Colors
        let brandBook = BrandBook.actual(model.mode)

        backgroundColor = brandBook.backgroundColor
        headerLabel.textColor = brandBook.processHeaderTextColor
        progressLabel.textColor = brandBook.processTextColor
        backgroundColor = brandBook.processBackgroundColor
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        headerLabel.pin
            .top(Specs.commonInsets.top * 4)
            .left(Specs.commonInsets.left)
            .width(frame.width - Specs.commonInsets.horizontalSum)
            .height(FontManager.h3FontSize + Specs.textInsets.verticalSum)

        progressLabel.pin
            .below(of: headerLabel)
            .marginTop(Specs.commonInsets.top)
            .left(Specs.commonInsets.left)
            .width(frame.width - Specs.commonInsets.horizontalSum)
            .height(FontManager.h1FontSize + Specs.textInsets.verticalSum)

        backgroundImage.pin
            .top(Specs.backgroundImageInsets.top)
            .right(Specs.backgroundImageInsets.right)
            .bottom(Specs.backgroundImageInsets.bottom)
            .width(frame.width / 2.0)
    }
}

private enum Specs {
    static let headerText = "Решено"
    static func progressText(done: Int, total: Int) -> String {
        return "\(done) / \(total) задач"
    }

    static let textInsets = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
    static let commonInsets = UIEdgeInsets(top: 8, left: 16, bottom: 16, right: 32)
    static let backgroundImageInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
}
