//
//  ProgressViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Tempura

struct ProgressVewModel: ViewModel {
    /// The number of tasks which are completed or closed
    let completed: Int

    /// The total number of tasks which user has ever created
    let total: Int

    let mode: AppState.Mode

    init(homeViewModel: HomeVewModel) {
        total = homeViewModel.tasks.count
        completed = homeViewModel.tasks.filter {
            $0.status == .completed || $0.status == .closed
        }.count
        mode = homeViewModel.mode
    }
}
