//
//  ProfileView.swift
//  needsupport
//
//  Created by Rita Konnova on 05/03/2022.
//

import PinLayout
import Tempura
import UIKit

class ProfileView: UIView, ViewControllerModellableView {
    // MARK: Subviews

    private let backgroundImage = UIImageView(frame: .zero)
    private let header = UILabel(frame: .zero)
    private let firstNameTextField = TextField(frame: .zero)
    private let phoneNumberTextField = TextField(frame: .zero)
    private let lastNameTextField = TextField(frame: .zero)
    private let logOutButton = UIButton(frame: .zero)
    private let imageView = UIImageView(frame: .zero)

    var didTapLogOutButton: (() -> Void)?
    var showAlertWithMessage: ((AlertConfiguration) -> Void)?

    // MARK: View Life Cycle

    func setup() {
        addSubview(backgroundImage)
        addSubview(header)
        addSubview(firstNameTextField)
        addSubview(phoneNumberTextField)
        addSubview(lastNameTextField)
        addSubview(logOutButton)
        addSubview(imageView)

        logOutButton.addTarget(self, action: #selector(logOutButtonTapped), for: .touchUpInside)
        lastNameTextField.autocapitalizationType = .none

        firstNameTextField.isUserInteractionEnabled = false
        phoneNumberTextField.isUserInteractionEnabled = false
        lastNameTextField.isUserInteractionEnabled = false
    }

    func style() {
        header.font = FontManager.bold(ofSize: FontManager.h1FontSize)
        firstNameTextField.font = FontManager.regular(ofSize: FontManager.basicTextFontSize)
        phoneNumberTextField.font = FontManager.regular(ofSize: FontManager.basicTextFontSize)
        lastNameTextField.font = FontManager.regular(ofSize: FontManager.basicTextFontSize)

        logOutButton.titleLabel?.font = FontManager.bold(ofSize: FontManager.h2FontSize)

        firstNameTextField.placeholder = Specs.namePlaceholder
        phoneNumberTextField.placeholder = Specs.phoneNumberPlaceholder
        phoneNumberTextField.keyboardType = .phonePad
        lastNameTextField.placeholder = Specs.emailPlaceholder
        lastNameTextField.keyboardType = .emailAddress

        logOutButton.setTitle(Specs.logOutButtonTitle, for: .normal)

        firstNameTextField.layer.masksToBounds = true
        phoneNumberTextField.layer.masksToBounds = true
        lastNameTextField.layer.masksToBounds = true
        logOutButton.layer.masksToBounds = true

        backgroundImage.contentMode = .scaleToFill
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 4.0
    }

    func update(oldModel _: ProfileViewModel?) {
        guard let model = model else {
            return
        }

        // MARK: Content Update

        switch model.screenType {
        case .logIn:
            header.text = Specs.registrationHeader
            backgroundImage.image = UIImage(named: "log-in-background")
        case let .crud(user):
            header.text = Specs.profileHeader
            guard let user = user else { break }
            firstNameTextField.text = user.firstName
            lastNameTextField.text = user.lastName
            phoneNumberTextField.text = user.phoneNumber
            backgroundImage.image = UIImage(named: "profile-background")

            let url = URL(string: user.pictureURLString)!
            if let data = try? Data(contentsOf: url) {
                imageView.image = UIImage(data: data)
                imageView.isHidden = false
            } else {
                imageView.isHidden = true
            }
        }

        // MARK: Colors Update

        let brandBook = BrandBook.actual(model.mode)
        backgroundColor = brandBook.backgroundColor

        firstNameTextField.backgroundColor = brandBook.inProcessBackgroundColor
        lastNameTextField.backgroundColor = brandBook.inProcessBackgroundColor
        phoneNumberTextField.backgroundColor = brandBook.inProcessBackgroundColor

        header.textColor = brandBook.firstTextColor
        firstNameTextField.textColor = brandBook.thirdTextColor
        phoneNumberTextField.textColor = brandBook.thirdTextColor
        lastNameTextField.textColor = brandBook.thirdTextColor
        logOutButton.backgroundColor = brandBook.disabledButtonBackgroundColor
        logOutButton.titleLabel?.textColor = brandBook.disabledButtonForegroundColor

        imageView.layer.borderColor = brandBook.imageBorderColor.cgColor
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        backgroundImage.frame = CGRect(x: 0, y: frame.height / 5, width: frame.width, height: frame.height)
        header.pin
            .top(Specs.safeAreaInsets.top)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(FontManager.h1FontSize + Specs.smallMargin)

        firstNameTextField.pin
            .below(of: header)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.textFieldHeight)

        lastNameTextField.pin
            .below(of: firstNameTextField)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.textFieldHeight)

        phoneNumberTextField.pin
            .below(of: lastNameTextField)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.textFieldHeight)

        imageView.pin
            .top(header.frame.midY - Specs.imageSize.height / 3)
            .right(Specs.commonInsets.right)
            .size(Specs.imageSize)
        imageView.layer.cornerRadius = imageView.frame.height / 2

        firstNameTextField.layer.cornerRadius = Specs.cornerRadius
        phoneNumberTextField.layer.cornerRadius = Specs.cornerRadius
        lastNameTextField.layer.cornerRadius = Specs.cornerRadius
        logOutButton.layer.cornerRadius = Specs.cornerRadius

        guard let screenType = model?.screenType else { return }
        switch screenType {
        case .logIn:
            break
        case .crud:
            logOutButton.pin
                .bottom(Specs.safeAreaInsets.bottom)
                .left(Specs.commonInsets.left)
                .right(Specs.commonInsets.right)
                .height(Specs.logInButtonHeight)
        }
    }

    // MARK: Validation

    func validateCredentials(withAlert shouldShowAlert: Bool = true) -> Bool {
        guard let mode = model?.mode else { return false }
        let validationCompletion: ((ValidationManager.Result) -> Bool) = { [weak self] result in
            guard let self = self else { return false }
            switch result {
            case .success:
                return true
            case let .error(message):
                if shouldShowAlert {
                    self.showAlertWithMessage?(
                        self.makeAlertConfiguration(
                            title: Specs.errorAlertTitle,
                            text: message,
                            buttonText: Specs.сloseAlertTitle,
                            mode: mode
                        )
                    )
                }
                return false
            }
        }

        var result = true
        result = result && validationCompletion(ValidationManager.validateName(firstNameTextField.text))
        result = result && validationCompletion(ValidationManager.validateName(lastNameTextField.text))
//        result = result && validationCompletion(ValidationManager.validateEmail(lastNameTextField.text))
        result = result && validationCompletion(ValidationManager.validatePhoneNumber(phoneNumberTextField.text))

        return result
    }

    // MARK: Interactions

    @objc func logOutButtonTapped() {
        didTapLogOutButton?()
    }
}

private enum Specs {
    static let safeAreaInsets = UIEdgeInsets(top: 70, left: 0, bottom: 64, right: 0)
    static let commonInsets = UIEdgeInsets(common: 32)
    static let smallMargin = CGFloat(16.0)
    static let imageSize = CGSize(width: 82, height: 82)
    static let registrationHeader = "Добро пожаловать"
    static let profileHeader = "Профиль"
    static let namePlaceholder = "Имя Фамилия"
    static let phoneNumberPlaceholder = "Телефон"
    static let emailPlaceholder = "E-mail"
    static let logInButtonTitle = "Войти"
    static let updateButtonTitle = "Сохранить"
    static let logOutButtonTitle = "Выйти"
    static let сloseAlertTitle = "Закрыть"
    static let errorAlertTitle = "Ошибка"
    static let logInButtonHeight = CGFloat(64.0)
    static let textFieldHeight = CGFloat(56.0)
    static let cornerRadius = CGFloat(16.0)
}
