//
//  ProfileViewController.swift
//  needsupport
//
//  Created by Rita Konnova on 05/03/2022.
//

import JSSAlertView
import Tempura

class ProfileViewController: ViewControllerWithLocalState<ProfileView> {
    // MARK: Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

        view.addGestureRecognizer(tap)
        updateMode()
    }

    // Calls this function when the tap is recognized
    @objc func dismissKeyboard() {
        // Causes the view (or one of its embedded text fields) to resign the first responder status
        view.endEditing(true)
    }

    // MARK: Logic

    override func setupInteraction() {
        rootView.didTapLogOutButton = {
            UserDefaults.standard.set(false, forKey: "HasOpenedHome")
            self.store.dispatch(ShowAuthorizationScreen())
        }

        rootView.showAlertWithMessage = { configuration in
            let alertView = JSSAlertView().show(
                self,
                title: configuration.title,
                text: configuration.text,
                buttonText: configuration.buttonText,
                color: configuration.backgroundColor
            )
            alertView.setTitleFont(configuration.titleFont.fontName)
            alertView.setTextFont(configuration.textFont.fontName)
            alertView.setButtonFont(configuration.buttonFont.fontName)
            alertView.setTextTheme(configuration.textTheme)
        }
    }

    // MARK: UI

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        guard
            traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle
        else {
            return
        }

        updateMode()
    }

    private func updateMode() {
        switch traitCollection.userInterfaceStyle {
        case .dark:
            dispatch(DarkModeStateUpdater())
        case .light, .unspecified:
            dispatch(LightModeStateUpdater())
        @unknown default:
            fatalError()
        }
    }
}
