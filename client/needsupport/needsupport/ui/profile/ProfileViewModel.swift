//
//  RegistrationViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 05/03/2022.
//

import Tempura

struct ProfileLocalState: LocalState {
    enum ScreenType {
        case logIn
        case crud(AppState.User?)
    }

    let type: ScreenType

    let mode: AppState.Mode
}

struct ProfileViewModel: ViewModelWithLocalState {
    typealias SS = AppState

    typealias LS = ProfileLocalState

    let screenType: ProfileLocalState.ScreenType

    let mode: AppState.Mode

    init?(state: AppState?, localState: ProfileLocalState) {
        screenType = localState.type
        mode = state?.mode ?? localState.mode
    }
}
