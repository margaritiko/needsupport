//
//  TasksManagementView.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import PinLayout
import Tempura
import UIKit

class TasksManagementView: UIView, ViewControllerModellableView, UITextViewDelegate {
    // MARK: Subviews
    
    private let header = UILabel(frame: .zero)
    private let categoryCollectionView = CarouselCollectionView(frame: .zero)
    private let titleHeader = UILabel(frame: .zero)
    private let titleTextArea = UITextView(frame: .zero)
    private let descriptionHeader = UILabel(frame: .zero)
    private let descriptionTextArea = UITextView(frame: .zero)
    private let commentHeader = UILabel(frame: .zero)
    private let commentTextArea = UITextView(frame: .zero)
    private let saveButton = Button(frame: .zero)
    
    // MARK: Actions
    
    var showAlertWithMessage: ((AlertConfiguration) -> Void)?
    var didTapButton: ((AppState.Task) -> Void)?
    var didTapCarouselChip: ((AppState.TaskCategory) -> Void)? {
        didSet {
            categoryCollectionView.didTapCell = didTapCarouselChip
        }
    }
    
    var didChangeTask: ((AppState.Task) -> Void)?
    
    var task: AppState.Task? {
        guard let category = model?.selectedCategory, let userID = model?.userID, let screenType = model?.screenType else { return nil }
        
        let taskId: String
        switch screenType {
        case .edition(let task):
            taskId = task.ID
        case .history(let task):
            taskId = task.ID
        case .creation:
            taskId = String()
        }
        
        return AppState.Task(
            ID: taskId,
            name: titleTextArea.text,
            description: descriptionTextArea.text,
            status: .created,
            clientID: userID,
            assigned: false,
            category: category,
            comment: commentTextArea.text
        )
    }
    
    // MARK: View Life Cycle
    
    func setup() {
        addSubview(header)
        addSubview(categoryCollectionView)
        addSubview(titleHeader)
        addSubview(titleTextArea)
        addSubview(descriptionHeader)
        addSubview(descriptionTextArea)
        addSubview(saveButton)
        
        saveButton.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        titleTextArea.delegate = self
        descriptionTextArea.delegate = self
    }
    
    func style() {
        header.font = FontManager.bold(ofSize: FontManager.h1FontSize)
        titleHeader.font = FontManager.bold(ofSize: FontManager.h2FontSize)
        descriptionHeader.font = FontManager.bold(ofSize: FontManager.h2FontSize)
        commentHeader.font = FontManager.bold(ofSize: FontManager.h2FontSize)
        
        titleTextArea.font = FontManager.regular(ofSize: FontManager.basicTextFontSize)
        descriptionTextArea.font = FontManager.regular(ofSize: FontManager.basicTextFontSize)
        commentTextArea.font = FontManager.regular(ofSize: FontManager.basicTextFontSize)
        
        saveButton.titleLabel?.font = FontManager.bold(ofSize: FontManager.h2FontSize)
        
        titleHeader.text = Specs.titleHeaderText
        descriptionHeader.text = Specs.descriptionHeaderText
        commentHeader.text = Specs.commentHeaderText
        
        titleTextArea.layer.masksToBounds = true
        descriptionTextArea.layer.masksToBounds = true
        commentTextArea.layer.masksToBounds = true
        
        saveButton.layer.masksToBounds = true
        
        titleTextArea.textContainerInset = UIEdgeInsets(common: 16)
        descriptionTextArea.textContainerInset = UIEdgeInsets(common: 16)
        commentTextArea.textContainerInset = UIEdgeInsets(common: 16)
    }
    
    func update(oldModel _: TasksManagementViewModel?) {
        guard let model = model else {
            return
        }
        
        // MARK: Content Update
        
        categoryCollectionView.model = CarouselViewModel(category: model.selectedCategory, mode: model.mode)
        
        switch model.screenType {
        case .creation:
            header.text = Specs.headerNewTaskText
            titleTextArea.isEditable = true
            descriptionTextArea.isEditable = true
            saveButton.setTitle(Specs.createButtonText, for: .normal)
            saveButton.isHidden = false
            saveButton.customEnabled = false
            categoryCollectionView.allowsSelection = true
        case let .edition(task):
            addSubview(commentHeader)
            addSubview(commentTextArea)
            
            commentTextArea.delegate = self
            
            header.text = Specs.headerUpdateText
            commentHeader.text = Specs.commentHeaderText
            
            titleTextArea.text = task.name
            descriptionTextArea.text = task.description
            
            titleTextArea.isEditable = true
            descriptionTextArea.isEditable = true
            commentTextArea.isEditable = true
            
            saveButton.setTitle(Specs.updateButtonText, for: .normal)
            saveButton.isHidden = false
            saveButton.customEnabled = true
            
            categoryCollectionView.allowsSelection = true
        case let .history(task):
            header.text = Specs.headerHistoryText
            titleTextArea.text = task.name
            descriptionTextArea.text = task.description
            titleTextArea.isEditable = false
            descriptionTextArea.isEditable = false
            saveButton.isHidden = true
            saveButton.customEnabled = false
            categoryCollectionView.allowsSelection = false
        }
        
        // MARK: Colors Update
        
        let brandBook = BrandBook.actual(model.mode)
        backgroundColor = brandBook.backgroundColor
        
        titleTextArea.backgroundColor = brandBook.inProcessBackgroundColor
        descriptionTextArea.backgroundColor = brandBook.inProcessBackgroundColor
        commentTextArea.backgroundColor = brandBook.inProcessBackgroundColor
        
        header.textColor = brandBook.firstTextColor
        titleHeader.textColor = brandBook.secondTextColor
        descriptionHeader.textColor = brandBook.secondTextColor
        commentHeader.textColor = brandBook.secondTextColor
        
        titleTextArea.textColor = brandBook.thirdTextColor
        descriptionTextArea.textColor = brandBook.thirdTextColor
        commentTextArea.textColor = brandBook.thirdTextColor
        
        saveButton.titleLabel?.textColor = brandBook.enabledButtonForegroundColor
        
        saveButton.setBackgroundColor(brandBook.enabledButtonBackgroundColor, for: .normal)
        saveButton.setBackgroundColor(brandBook.disabledButtonBackgroundColor, for: .disabled)
        if !saveButton.customEnabled {
            saveButton.backgroundColor = brandBook.disabledButtonBackgroundColor
        } else {
            saveButton.backgroundColor = brandBook.enabledButtonBackgroundColor
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        header.pin
            .top(Specs.safeAreaInsets.top)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(FontManager.h1FontSize + Specs.smallMargin)
        
        categoryCollectionView.pin
            .below(of: header)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.categoryChipHeight)
        
        titleHeader.pin
            .below(of: categoryCollectionView)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(FontManager.h2FontSize)
        
        titleTextArea.pin
            .below(of: titleHeader)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.titleTextAreaHeight / 2)
        
        descriptionHeader.pin
            .below(of: titleTextArea)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(FontManager.h2FontSize)
        
        guard let screenType = model?.screenType else { return }
        if case .history = screenType {
            saveButton.pin
                .bottom(Specs.safeAreaInsets.bottom)
                .left(Specs.commonInsets.left)
                .right(Specs.commonInsets.right)
                .height(1.0)
        } else {
            saveButton.pin
                .bottom(Specs.safeAreaInsets.bottom)
                .left(Specs.commonInsets.left)
                .right(Specs.commonInsets.right)
                .height(Specs.saveButtonHeight)
        }
        
        commentHeader.pin
            .below(of: descriptionTextArea)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(FontManager.h2FontSize)
        
        commentTextArea.pin
            .below(of: commentHeader)
            .above(of: saveButton)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.titleTextAreaHeight / 2)
        
        if case .creation = screenType {
            descriptionTextArea.pin
                .below(of: descriptionHeader)
                .marginTop(Specs.smallMargin)
                .above(of: saveButton)
                .marginBottom(Specs.smallMargin * 2)
                .left(Specs.commonInsets.left)
                .right(Specs.commonInsets.right)
        } else {
            descriptionTextArea.pin
                .below(of: descriptionHeader)
                .marginTop(Specs.smallMargin)
                .above(of: saveButton)
                .marginBottom(Specs.smallMargin * 2)
                .left(Specs.commonInsets.left)
                .right(Specs.commonInsets.right)
                .height(Specs.titleTextAreaHeight * 1.2)
        }
        
        titleTextArea.layer.cornerRadius = Specs.cardCornerRadius
        descriptionTextArea.layer.cornerRadius = Specs.cardCornerRadius
        commentTextArea.layer.cornerRadius = Specs.cardCornerRadius
        saveButton.layer.cornerRadius = Specs.cardCornerRadius
    }
    
    // MARK: Logic
    
    func validateTask(withAlert shouldShowAlert: Bool = true) -> Bool {
        guard let mode = model?.mode else { return false }
        switch ValidationManager.validateTaskTitle(titleTextArea.text) {
        case .success:
            break
        case let .error(message):
            if shouldShowAlert {
                showAlertWithMessage?(
                    makeAlertConfiguration(
                        title: Specs.errorAlertTitle,
                        text: message,
                        buttonText: Specs.сloseAlertTitle,
                        mode: mode
                    )
                )
            }
            return false
        }
        
        switch ValidationManager.validateTaskDescription(descriptionTextArea.text) {
        case .success:
            break
        case let .error(message):
            if shouldShowAlert {
                showAlertWithMessage?(
                    makeAlertConfiguration(
                        title: Specs.errorAlertTitle,
                        text: message,
                        buttonText: Specs.сloseAlertTitle,
                        mode: mode
                    )
                )
            }
            return false
        }
        
        if categoryCollectionView.model?.selectedCategory == nil {
            if shouldShowAlert {
                showAlertWithMessage?(
                    makeAlertConfiguration(
                        title: Specs.errorAlertTitle,
                        text: "Категория задачи не выбрана",
                        buttonText: Specs.сloseAlertTitle,
                        mode: mode
                    )
                )
            }
            return false
        }
        
        return true
    }
    
    func textViewDidChange(_: UITextView) {
        guard let task = task else { return }
        didChangeTask?(task)
        
        if validateTask(withAlert: false) {
            saveButton.customEnabled = true
        } else {
            saveButton.customEnabled = false
        }
    }
    
    @objc func buttonTapped() {
        guard validateTask() else { return }
        guard let task = task else { return }
        didTapButton?(task)
    }
}

private enum Specs {
    static let safeAreaInsets = UIEdgeInsets(top: 42, left: 0, bottom: 64, right: 0)
    static let commonInsets = UIEdgeInsets(common: 32)
    static let smallMargin = CGFloat(16.0)
    static let headerNewTaskText = "Новая задача"
    static let headerUpdateText = "Редактировать"
    static let headerHistoryText = "Просмотр"
    static let titleHeaderText = "Название"
    static let descriptionHeaderText = "Описание"
    static let commentHeaderText = "Комментарий"
    static let titleTextAreaText = "Введите краткое название задачи"
    static let descriptionTextAreaText = "Пожалуйста, постарайтесь предоставить как можно больше деталей"
    static let commentTextAreaText = "Пожалуйста, введите комментарий к изменениям"
    static let createButtonText = "Создать"
    static let updateButtonText = "Сохранить изменения"
    static let сloseAlertTitle = "Закрыть"
    static let errorAlertTitle = "Ошибка"
    static var titleTextAreaHeight: CGFloat {
        if UIScreen.main.nativeBounds.height < 1334 {
            return CGFloat(70)
        } else {
            return CGFloat(140)
        }
    }
    
    static let saveButtonHeight = CGFloat(64.0)
    static let cardCornerRadius = CGFloat(24.0)
    static let categoryChipHeight = CGFloat(42.0)
}
