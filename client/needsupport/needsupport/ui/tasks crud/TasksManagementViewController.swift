//
//  TasksManagementViewController.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import JSSAlertView
import Tempura

class TasksManagementViewController: ViewControllerWithLocalState<TasksManagementView> {
    // MARK: Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

        view.addGestureRecognizer(tap)
    }

    // Calls this function when the tap is recognized
    @objc func dismissKeyboard() {
        // Causes the view (or one of its embedded text fields) to resign the first responder status
        view.endEditing(true)
    }

    // MARK: Logic

    override func didUpdateLocalState() {
        rootView.model = TasksManagementViewModel(state: state, localState: localState)
    }

    override func setupInteraction() {
        rootView.didTapButton = { [weak self] task in
            guard let self = self else { return }
            switch self.localState.type {
            case .creation:
                self.store.dispatch(TaskPoster(task: task))
                self.store.dispatch(DataFetcher())
            case let .edition(oldTask):
                self.store.dispatch(TaskUpdater(task: oldTask))
                self.localState = TasksManagementLocalState(
                    type: .edition(task),
                    selectedCategory: task.category,
                    mode: self.localState.mode
                )
            case .history:
                return
            }

            self.store.dispatch(ShowHomeScreen())
        }

        rootView.didChangeTask = { [weak self] task in
            guard let self = self else { return }
            switch self.localState.type {
            case .creation:
                return
            case let .edition(oldTask):
                self.store.dispatch(UpdateTaskStateUpdater(oldTask: oldTask, newTask: task))
                self.localState = TasksManagementLocalState(
                    type: .edition(task),
                    selectedCategory: task.category,
                    mode: self.localState.mode
                )
            case .history:
                return
            }
        }

        rootView.showAlertWithMessage = { configuration in
            let alertView = JSSAlertView().show(
                self,
                title: configuration.title,
                text: configuration.text,
                buttonText: configuration.buttonText,
                color: configuration.backgroundColor
            )
            alertView.setTitleFont(configuration.titleFont.fontName)
            alertView.setTextFont(configuration.textFont.fontName)
            alertView.setButtonFont(configuration.buttonFont.fontName)
            alertView.setTextTheme(configuration.textTheme)
        }

        rootView.didTapCarouselChip = { [weak self] category in
            self?.localState.selectedCategory = category
        }
    }
}
