//
//  TasksManagementViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 24/02/2022.
//

import Tempura

struct TasksManagementLocalState: LocalState {
    enum ScreenType {
        case creation
        case edition(AppState.Task)
        case history(AppState.Task)
    }

    let type: ScreenType

    var selectedCategory: AppState.TaskCategory?

    let mode: AppState.Mode
}

struct TasksManagementViewModel: ViewModelWithLocalState {
    typealias SS = AppState

    typealias LS = TasksManagementLocalState

    let screenType: TasksManagementLocalState.ScreenType

    let mode: AppState.Mode

    let selectedCategory: AppState.TaskCategory?

    let userID: Int

    init?(state: AppState?, localState: TasksManagementLocalState) {
        screenType = localState.type
        mode = state?.mode ?? localState.mode
        selectedCategory = .task
        userID = state?.user?.ID ?? 0
    }
}
