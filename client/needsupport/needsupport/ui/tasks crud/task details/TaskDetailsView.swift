//
//  TaskDetailsView.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import GTProgressBar
import PinLayout
import Tempura
import UIKit

class TaskDetailsView: UIView, ViewControllerModellableView, UITextViewDelegate {
    // MARK: Subviews

    private let backButton = UIButton(frame: .zero)
    private let progressBar = GTProgressBar(frame: .zero)
    private let progressValue = UILabel(frame: .zero)
    private let progressStatus = UILabel(frame: .zero)
    private let title = UILabel(frame: .zero)
    private let category = UILabel(frame: .zero)
    private let details = UITextView(frame: .zero)
    private let editButton = UIButton(frame: .zero)
    private let deleteButton = UIButton(frame: .zero)
    private let showEventsButton = UIButton(frame: .zero)

    // MARK: Actions

    var didTapBackButton: (() -> Void)?
    var didTapEditButton: (() -> Void)?
    var didTapDeleteButton: (() -> Void)?
    var didTapShowEventsButton: (() -> Void)?

    // MARK: View Life Cycle

    func setup() {
        addSubview(backButton)
        addSubview(progressBar)
        addSubview(progressValue)
        addSubview(progressStatus)
        addSubview(title)
        addSubview(category)
        addSubview(details)
        addSubview(editButton)
        addSubview(deleteButton)
        addSubview(showEventsButton)

        title.numberOfLines = 0

        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        editButton.addTarget(self, action: #selector(editButtonTapped), for: .touchUpInside)
        deleteButton.addTarget(self, action: #selector(deleteButtonTapped), for: .touchUpInside)
        showEventsButton.addTarget(self, action: #selector(showEventsButtonTapped), for: .touchUpInside)
    }

    func style() {
        title.font = FontManager.bold(ofSize: FontManager.h1FontSize)
        progressValue.font = FontManager.bold(ofSize: FontManager.h2FontSize)
        progressStatus.font = FontManager.bold(ofSize: FontManager.h3FontSize)
        category.font = FontManager.light(ofSize: FontManager.basicTextFontSize)
        details.font = FontManager.regular(ofSize: FontManager.h3FontSize)
        details.showsVerticalScrollIndicator = false

        editButton.contentVerticalAlignment = .fill
        editButton.contentHorizontalAlignment = .fill
        editButton.imageEdgeInsets = Specs.buttonInsets
        editButton.layer.masksToBounds = true

        deleteButton.contentVerticalAlignment = .fill
        deleteButton.contentHorizontalAlignment = .fill
        deleteButton.imageEdgeInsets = Specs.buttonInsets
        deleteButton.layer.masksToBounds = true

        showEventsButton.contentVerticalAlignment = .fill
        showEventsButton.contentHorizontalAlignment = .fill
        showEventsButton.imageEdgeInsets = Specs.buttonInsets
        showEventsButton.layer.masksToBounds = true

        backButton.contentVerticalAlignment = .fill
        backButton.contentHorizontalAlignment = .fill
        backButton.imageEdgeInsets = Specs.buttonInsets

        details.isEditable = false
        progressBar.progress = 0
        progressBar.barBorderWidth = 1
        progressBar.barFillInset = 2
        progressBar.barBorderColor = .clear
        progressBar.direction = GTProgressBarDirection.clockwise
        progressBar.displayLabel = false
        progressBar.barBorderWidth = 0
        details.backgroundColor = .clear
    }

    func update(oldModel _: TaskDetailsViewModel?) {
        guard let model = model else {
            return
        }

        // MARK: Content Update

        title.text = model.task.name
        details.text = model.task.description
        progressStatus.text = model.task.status.description
        progressValue.text = "\(model.task.status.integerProgress)%"

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            self?.progressBar.animateTo(progress: CGFloat(model.task.status.integerProgress) / 100.0)
        }

        if model.canEdit {
            deleteButton.isHidden = false
            editButton.isHidden = false
        } else {
            deleteButton.isHidden = true
            editButton.isHidden = true
        }

        // MARK: Colors Update

        let brandBook = BrandBook.actual(model.mode)
        backgroundColor = brandBook.backgroundColor

        title.textColor = brandBook.firstTextColor
        category.textColor = brandBook.thirdTextColor
        details.textColor = brandBook.thirdTextColor
        progressValue.textColor = brandBook.progressBarForegroundColor
        progressStatus.textColor = brandBook.progressStatusTextColor
        editButton.backgroundColor = brandBook.editTaskButtonBackgroundColor
        deleteButton.backgroundColor = brandBook.deleteTaskButtonBackgroundColor
        showEventsButton.backgroundColor = brandBook.editTaskButtonBackgroundColor

        progressBar.barFillColor = brandBook.progressBarForegroundColor
        progressBar.barBackgroundColor = brandBook.progressBarBackgroundColor

        let categoryText = "Категория: \(model.task.category.description)"
        let range = (categoryText as NSString).range(of: model.task.category.description)

        let mutableAttributedString = NSMutableAttributedString(string: categoryText)
        mutableAttributedString.addAttribute(
            NSAttributedString.Key.foregroundColor,
            value: brandBook.progressBarForegroundColor,
            range: range
        )
        category.attributedText = mutableAttributedString

        switch model.mode {
        case .dark:
            editButton.setImage(UIImage(named: "edit-task-dark"), for: .normal)
            deleteButton.setImage(UIImage(named: "delete-task-dark"), for: .normal)
            backButton.setImage(UIImage(named: "go-back-to-home-dark"), for: .normal)
            showEventsButton.setImage(UIImage(named: "task-history-dark"), for: .normal)
        case .light:
            editButton.setImage(UIImage(named: "edit-task-light"), for: .normal)
            deleteButton.setImage(UIImage(named: "delete-task-light"), for: .normal)
            backButton.setImage(UIImage(named: "go-back-to-home-light"), for: .normal)
            showEventsButton.setImage(UIImage(named: "task-history-light"), for: .normal)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        guard let model = model else { return }

        backButton.pin
            .left(0)
            .top(Specs.safeAreaInsets.top)
            .size(Specs.backButtonSize)

        showEventsButton.pin
            .right(Specs.commonInsets.right)
            .bottom(Specs.commonInsets.bottom)
            .size(Specs.toolButtonSize)

        editButton.pin
            .above(of: showEventsButton)
            .marginBottom(Specs.smallMargin)
            .right(Specs.commonInsets.right)
            .size(Specs.toolButtonSize)

        deleteButton.pin
            .above(of: editButton)
            .marginBottom(Specs.smallMargin)
            .right(Specs.commonInsets.right)
            .size(Specs.toolButtonSize)

        // We set width to restrict the view size when `sizeToFit` will be called
        title.frame = CGRect(x: 0, y: 0, width: frame.width - Specs.commonInsets.horizontalSum, height: 100)
        title.sizeToFit()
        let titleHeight = title.frame.height
        title.pin
            .below(of: backButton)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(titleHeight)

        category.pin
            .below(of: title)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(FontManager.h3FontSize)

        progressBar.pin
            .below(of: category)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.progressBarHeight)

        progressValue.sizeToFit()
        let progressValueWidth = progressValue.frame.width
        progressValue.pin
            .below(of: progressBar)
            .marginTop(Specs.smallMargin)
            .left(Specs.commonInsets.left)
            .width(progressValueWidth)
            .height(FontManager.h2FontSize)

        progressStatus.pin
            .below(of: progressBar)
            .marginTop(Specs.smallMargin)
            .marginLeft(Specs.smallMargin)
            .right(of: progressValue)
            .right(Specs.commonInsets.right)
            .height(FontManager.h2FontSize)

        if model.canEdit {
            details.pin
                .below(of: progressValue)
                .marginTop(Specs.smallMargin)
                .left(Specs.commonInsets.left - Specs.leftShift)
                .left(of: editButton)
                .marginRight(Specs.smallMargin)
                .bottom(Specs.commonInsets.bottom)
        } else {
            details.pin
                .below(of: progressValue)
                .marginTop(Specs.smallMargin)
                .left(Specs.commonInsets.left - Specs.leftShift)
                .right(Specs.commonInsets.right)
                .bottom(Specs.commonInsets.bottom)
        }

        editButton.layer.cornerRadius = editButton.frame.height / 2
        deleteButton.layer.cornerRadius = deleteButton.frame.height / 2
        showEventsButton.layer.cornerRadius = showEventsButton.frame.height / 2
    }

    // MARK: Logic

    @objc func backButtonTapped() {
        didTapBackButton?()
    }

    @objc func editButtonTapped() {
        didTapEditButton?()
    }

    @objc func deleteButtonTapped() {
        didTapDeleteButton?()
    }

    @objc func showEventsButtonTapped() {
        didTapShowEventsButton?()
    }
}

private enum Specs {
    static let safeAreaInsets = UIEdgeInsets(top: 42, left: 0, bottom: 64, right: 0)
    static let commonInsets = UIEdgeInsets(common: 32)
    static let smallMargin = CGFloat(16.0)
    static let buttonInsets = UIEdgeInsets(common: 12)
    static let toolButtonSize = CGFloat(64.0)
    static let backButtonSize = CGFloat(92.0)
    static let progressBarHeight = CGFloat(16.0)
    // Because of the shift inside `UITextArea`
    static let leftShift = CGFloat(4.0)
}
