//
//  TaskDetailsViewController.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import Tempura

class TaskDetailsViewController: ViewControllerWithLocalState<TaskDetailsView> {
    // MARK: Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: Logic

    override func setupInteraction() {
        rootView.didTapBackButton = {
            self.store.dispatch(Hide())
        }

        rootView.didTapEditButton = { [weak self] in
            guard let self = self else { return }
            let stateUpdater = ShowEditScreen(task: self.localState.selectedTask)
            self.store.dispatch(stateUpdater)
        }

        rootView.didTapDeleteButton = { [weak self] in
            guard let self = self else { return }
            let task = self.localState.selectedTask
            let stateUpdater = UpdateTaskStateUpdater(
                oldTask: task,
                newTask: AppState.Task(
                    ID: task.ID,
                    name: task.name,
                    description: task.description,
                    status: .closed,
                    clientID: task.clientID,
                    assigned: task.assigned,
                    category: task.category
                )
            )
            self.store.dispatch(stateUpdater)
            self.store.dispatch(Hide())
        }

        rootView.didTapShowEventsButton = { [weak self] in
            guard let self = self else { return }
            let task = self.localState.selectedTask
            self.dispatch(TaskEventFetcher())
            let stateUpdater = ShowTaskEventsScreen(taskEvents: self.state.taskEvents.filter { $0.taskID == task.ID })
            self.store.dispatch(stateUpdater)
        }
    }

    // MARK: UI

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        guard
            traitCollection.userInterfaceStyle != previousTraitCollection?.userInterfaceStyle
        else {
            return
        }

        updateMode()
    }

    private func updateMode() {
        switch traitCollection.userInterfaceStyle {
        case .dark:
            dispatch(DarkModeStateUpdater())
        case .light, .unspecified:
            dispatch(LightModeStateUpdater())
        @unknown default:
            fatalError()
        }
    }
}
