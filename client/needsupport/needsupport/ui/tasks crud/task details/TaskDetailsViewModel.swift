//
//  TaskDetailsViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import Tempura

struct TasksDetailsLocalState: LocalState {
    enum ScreenType {
        case editing
        case reading
    }

    let type: ScreenType

    var selectedTask: AppState.Task

    let mode: AppState.Mode
}

struct TaskDetailsViewModel: ViewModelWithLocalState {
    typealias SS = AppState

    typealias LS = TasksDetailsLocalState

    let mode: AppState.Mode

    let task: AppState.Task

    let screenType: TasksDetailsLocalState.ScreenType

    var canEdit: Bool {
        return screenType == .editing
    }

    init?(state: AppState?, localState: TasksDetailsLocalState) {
        mode = state?.mode ?? localState.mode
        task = localState.selectedTask
        screenType = localState.type
    }
}
