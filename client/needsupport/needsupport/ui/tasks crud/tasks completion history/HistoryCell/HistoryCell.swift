//
//  HistoryCell.swift
//  needsupport
//
//  Created by Rita Konnova on 24/03/2022.
//

import Tempura
import UIKit

class HistoryCell: UICollectionViewCell, ModellableView {
    private let category = UILabel(frame: .zero)
    private let title = UILabel(frame: .zero)

    override init(frame _: CGRect) {
        super.init(frame: .zero)

        setup()
        style()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: `ModellableView`

    func setup() {
        addSubview(category)
        addSubview(title)
    }

    func style() {
        category.font = FontManager.regular(ofSize: FontManager.basicTextFontSize)
        title.font = FontManager.bold(ofSize: FontManager.h3FontSize)
        title.numberOfLines = 0
        layer.cornerRadius = Specs.cellCornerRadius
        layer.masksToBounds = true
        category.layer.masksToBounds = true
    }

    func update(oldModel _: HistoryViewModel?) {
        guard let model = model else { return }

        // Content

        category.text = model.taskEvent.type
        
        switch model.taskEvent.type {
        case "COMMENT":
            category.text = "Обновление информации"
        case "SYSTEM_EVENT":
            category.text = "Системное событие"
        case "QUESTION_FOR_CLIENT":
            category.text = "Вопрос"
        case "INFO_UPDATE":
            category.text = "Комментарий"
        default:
            category.text = model.taskEvent.type
        }
        
        title.text = model.taskEvent.comment

        // Colors

        let brandBook = BrandBook.actual(model.mode)

        if category.text == "Комментарий" {
            backgroundColor = brandBook.processBackgroundColor
            category.textColor = .lightGray
            title.textColor = .white
        } else {
            category.textColor = brandBook.categoryTextColor
            title.textColor = brandBook.secondTextColor
            backgroundColor = brandBook.inProcessBackgroundColor
        }
    }

    func heightThatFits(width: CGFloat) -> CGFloat {
        title.frame = CGRect(x: 0, y: 0, width: width, height: 64)
        title.sizeToFit()
        let titleHeight = title.frame.height

        return Specs.commonInsets.verticalSum * 2 + titleHeight + Specs.smallTextInsets.verticalSum + FontManager.basicTextFontSize
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        category.pin
            .top(Specs.commonInsets.top)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(Specs.smallTextInsets.verticalSum + FontManager.basicTextFontSize)

        title.frame = CGRect(x: 0, y: 0, width: frame.width - Specs.commonInsets.horizontalSum, height: 0)
        title.sizeToFit()
        let titleHeight = title.frame.height
        title.pin
            .below(of: category)
            .marginTop(Specs.commonMargin)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .height(titleHeight)
    }
}

private enum Specs {
    static let commonInsets = UIEdgeInsets(top: 8, left: 16, bottom: 16, right: 16)
    static let commonMargin = CGFloat(4.0)
    static let smallTextInsets = UIEdgeInsets(common: 2)
    static let cellCornerRadius = CGFloat(16.0)
}

