//
//  HistoryViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 24/03/2022.
//

import Tempura

struct HistoryViewModel: ViewModel {
    let taskEvent: AppState.TaskEvent

    let mode: AppState.Mode

    init(taskEvent: AppState.TaskEvent, mode: AppState.Mode) {
        self.taskEvent = taskEvent
        self.mode = mode
    }
}
