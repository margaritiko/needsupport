//
//  TaskHistoryView.swift
//  needsupport
//
//  Created by Rita Konnova on 24/03/2022.
//

import GTProgressBar
import PinLayout
import Tempura
import UIKit

class TaskHistoryView: UIView, ViewControllerModellableView, UITextViewDelegate {
    // MARK: Subviews

    private let collectionView: UICollectionView

    // MARK: View Life Cycle

    override init(frame: CGRect) {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)

        super.init(frame: frame)

        setup()
        style()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        addSubview(collectionView)
        collectionView.register(HistoryCell.self, forCellWithReuseIdentifier: Constants.taskHistoryCellReuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    func style() {
    }

    func update(oldModel _: TaskHistoryViewModel?) {
        guard let model = model else {
            return
        }

        let brandBook = BrandBook.actual(model.mode)
        backgroundColor = brandBook.backgroundColor
        collectionView.backgroundColor = brandBook.backgroundColor
        collectionView.reloadData()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        collectionView.pin
            .top(Specs.commonInsets.top)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .bottom(Specs.commonInsets.bottom)
    }
}

extension TaskHistoryView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let model = model else {
            return CGSize(width: self.frame.width, height: 64)
        }
        let width = self.frame.width - Specs.commonInsets.horizontalSum
        let cell = HistoryCell(frame: .zero)
        cell.model = HistoryViewModel(taskEvent: model.taskEvents[indexPath.row], mode: model.mode)
        return CGSize(width: width, height: cell.heightThatFits(width: width))
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model?.taskEvents.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard
            let model = model,
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: Constants.taskHistoryCellReuseIdentifier,
                for: indexPath
            ) as? HistoryCell
        else {
            fatalError("Wrong class for cell")
        }

        cell.model = HistoryViewModel(taskEvent: model.taskEvents[indexPath.row], mode: model.mode)

        return cell
    }
}

private enum Constants {
    static let taskHistoryCellReuseIdentifier = "TaskHistory"
}

private enum Specs {
    static let safeAreaInsets = UIEdgeInsets(top: 42, left: 0, bottom: 64, right: 0)
    static let commonInsets = UIEdgeInsets(common: 32)
    static let smallMargin = CGFloat(16.0)
    static let buttonInsets = UIEdgeInsets(common: 12)
    static let toolButtonSize = CGFloat(64.0)
    static let backButtonSize = CGFloat(92.0)
    static let progressBarHeight = CGFloat(16.0)
    // Because of the shift inside `UITextArea`
    static let leftShift = CGFloat(4.0)
}

