//
//  TaskHistoryViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 24/03/2022.
//

import Tempura

struct TaskHistoryLocalState: LocalState {
    var taskEvents: [AppState.TaskEvent]

    let mode: AppState.Mode
}

struct TaskHistoryViewModel: ViewModelWithLocalState {
    typealias SS = AppState

    let mode: AppState.Mode

    let taskEvents: [AppState.TaskEvent]

    init?(state: AppState?, localState: TaskHistoryLocalState) {
        mode = state?.mode ?? localState.mode
        taskEvents = localState.taskEvents

//        taskEvents = [
//            AppState.TaskEvent(comment: "Первый комментарий", kind: "", userID: 0, taskID: "", type: "Автоматическое обновление"),
//            AppState.TaskEvent(comment: "Второй комментарий. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.", kind: "", userID: 0, taskID: "", type: "Автоматическое обновление"),
//            AppState.TaskEvent(comment: "Третий комментарий", kind: "", userID: 0, taskID: "", type: "Комментарий"),
//            AppState.TaskEvent(comment: "Четвертый комментарий", kind: "", userID: 0, taskID: "", type: "Автоматическое обновление"),
//            AppState.TaskEvent(comment: "Первый комментарий", kind: "", userID: 0, taskID: "", type: "Автоматическое обновление"),
//            AppState.TaskEvent(comment: "Второй комментарий. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.", kind: "", userID: 0, taskID: "", type: "Автоматическое обновление"),
//            AppState.TaskEvent(comment: "Третий комментарий", kind: "", userID: 0, taskID: "", type: "Комментарий"),
//            AppState.TaskEvent(comment: "Четвертый комментарий", kind: "", userID: 0, taskID: "", type: "Автоматическое обновление"),
//        ]
    }
}

