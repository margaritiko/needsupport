//
//  Button.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import UIKit

class Button: UIButton {
    enum ButtonState {
        case normal
        case disabled
    }

    private var disabledBackgroundColor: UIColor?

    private var defaultBackgroundColor: UIColor? {
        didSet {
            backgroundColor = defaultBackgroundColor
        }
    }

    var customEnabled: Bool = true {
        didSet {
            if customEnabled {
                if let color = defaultBackgroundColor {
                    layer.backgroundColor = color.cgColor
                }
            } else {
                if let color = disabledBackgroundColor {
                    layer.backgroundColor = color.cgColor
                }
            }
        }
    }

    func setBackgroundColor(_ color: UIColor?, for state: ButtonState) {
        switch state {
        case .disabled:
            disabledBackgroundColor = color
        case .normal:
            defaultBackgroundColor = color
        }
    }
}
