//
//  CarouselCollectionView.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import Tempura
import UIKit

class CarouselCollectionView: UIView, ModellableView {
    private var collectionView: UICollectionView

    var didTapCell: ((AppState.TaskCategory) -> Void)?

    var allowsSelection: Bool = true {
        didSet {
            self.collectionView.allowsSelection = allowsSelection
        }
    }

    override init(frame: CGRect) {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)

        super.init(frame: frame)

        setup()
        style()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.selectItem(at: .init(row: 0, section: 0), animated: true, scrollPosition: .left)
        collectionView.register(
            ChipView.self,
            forCellWithReuseIdentifier: Constants.credentialsCellReuseIdentifier
        )

        addSubview(collectionView)
        collectionView.allowsSelection = true
        collectionView.isUserInteractionEnabled = true
    }

    func style() {
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        backgroundColor = .clear
    }

    func update(oldModel _: CarouselViewModel?) {
        DispatchQueue.main.async {
            self.layoutIfNeeded()
            self.collectionView.reloadData()
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        collectionView.pin.all()
    }
}

extension CarouselCollectionView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(
        _: UICollectionView,
        numberOfItemsInSection _: Int
    ) -> Int {
        return AppState.TaskCategory.allCases.count
    }

    func collectionView(
        _: UICollectionView,
        layout _: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let textLength = CGFloat(AppState.TaskCategory.allCases[indexPath.row].description.count)
        return CGSize(
            width: textLength * FontManager.basicTextFontSize,
            height: frame.height - Specs.commonInsets.verticalSum
        )
    }

    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        guard
            let model = model,
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: Constants.credentialsCellReuseIdentifier,
                for: indexPath
            ) as? ChipView
        else {
            fatalError("Wrong class for cell")
        }

        let categories = AppState.TaskCategory.allCases
        guard categories.count >= indexPath.row else {
            fatalError("No category was found at index \(indexPath.row)")
        }

        cell.model = ChipViewModel(
            category: categories[indexPath.row],
            isSelected: model.selectedCategory == categories[indexPath.row],
            mode: model.mode
        )

        cell.didTapCell = didTapCell

        return cell
    }
}

private enum Constants {
    static let credentialsCellReuseIdentifier = "Chip"
}

private enum Specs {
    static let commonInsets = UIEdgeInsets(common: 4)
}
