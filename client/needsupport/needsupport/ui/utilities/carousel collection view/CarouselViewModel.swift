//
//  CarouselViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import Tempura

struct CarouselViewModel: ViewModel {
    let selectedCategory: AppState.TaskCategory?

    let mode: AppState.Mode

    init(category _: AppState.TaskCategory?, mode: AppState.Mode) {
        selectedCategory = .task
        self.mode = mode
    }
}
