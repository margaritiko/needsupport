//
//  ChipView.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import Tempura
import UIKit

class ChipView: UICollectionViewCell, ModellableView {
    private let title = UILabel(frame: .zero)

    var didTapCell: ((AppState.TaskCategory) -> Void)?

    override init(frame _: CGRect) {
        super.init(frame: .zero)

        setup()
        style()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: `ModellableView`

    func setup() {
        addSubview(title)
        title.textAlignment = .center

        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapChip(_:))))
    }

    @objc func didTapChip(_: UICollectionViewCell) {
        guard let category = model?.category else { return }
        didTapCell?(category)
    }

    func style() {
        title.font = FontManager.bold(ofSize: FontManager.h3FontSize)
        layer.masksToBounds = true
    }

    func update(oldModel _: ChipViewModel?) {
        guard let model = model else { return }

        // Content

        title.text = model.category.description

        // Colors

        let brandBook = BrandBook.actual(model.mode)
        title.textColor = brandBook.secondTextColor
        if model.isSelected {
            backgroundColor = brandBook.selectedChipBackgroundColor
            title.textColor = brandBook.selectedChipForegroundColor
        } else {
            backgroundColor = brandBook.notSelectedChipBackgroundColor
            title.textColor = brandBook.notSelectedChipForegroundColor
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        title.pin
            .top(Specs.commonInsets.top)
            .left(Specs.commonInsets.left)
            .right(Specs.commonInsets.right)
            .bottom(Specs.commonInsets.bottom)

        layer.cornerRadius = frame.height / 2
    }
}

private enum Specs {
    static let commonInsets = UIEdgeInsets(common: 4)
}
