//
//  ChipViewModel.swift
//  needsupport
//
//  Created by Rita Konnova on 11/03/2022.
//

import Tempura

struct ChipViewModel: ViewModel {
    let category: AppState.TaskCategory

    let isSelected: Bool

    let mode: AppState.Mode

    init(category: AppState.TaskCategory, isSelected: Bool, mode: AppState.Mode) {
        self.category = category
        self.mode = mode
        self.isSelected = isSelected
    }
}
