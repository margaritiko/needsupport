//
//  needsupportTests.swift
//  needsupportTests
//
//  Created by Rita Konnova on 24/02/2022.
//

@testable import needsupport
import XCTest

class needsupportTests: XCTestCase {
    func createUrl(path: String, queryItems: [(name: String, value: String)]) -> String {
        var components = URLComponents()
        components.scheme = Settings.scheme
        components.host = Settings.host
        components.path = path
        var queryItemsArray: [URLQueryItem] = []
        for queryItem in queryItems {
            queryItemsArray.append(URLQueryItem(name: queryItem.name, value: queryItem.value))
        }
        components.queryItems = queryItemsArray
        
        return components.string!
    }
    
    func testGettingAllUserTasks_succeed() throws {
        let creatorId: Int = 505066172
        guard let url = URL(string: createUrl(path: "/api/tasks", queryItems: [("filter", "creator=\(String(creatorId))")])) else { return }

        var request = URLRequest(url: url)
        request.setValue(Settings.cookieHeaderValue, forHTTPHeaderField: "Cookie")
        request.setValue(Settings.acceptHeaderValue, forHTTPHeaderField: "Accept")
        
        let cancelExpectation = expectation(description: "All tasks by creator with id \(creatorId)")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                do {
                    guard
                        let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                        let tasks = json["data"] as? [[String: Any]]
                    else {
                        fatalError()
                    }
                    XCTAssertEqual(tasks[0]["creator"] as? Int, creatorId)
                } catch{
                    fatalError()
                }
                cancelExpectation.fulfill()
            } else if let error = error {
                fatalError(error.localizedDescription)
            }
        }
        
        task.resume()
        waitForExpectations(timeout: 1, handler: nil)
    }
    
    func testGettingMainTaskStatus_succeed() throws {
        let creatorId: Int = 505066172
        let taskId: String = "62335cdc0e90559fc6a13a18"
        guard let url = URL(string: createUrl(path: "/api/tasks", queryItems: [("filter", "creator=\(String(creatorId))")])) else { return }

        var request = URLRequest(url: url)
        request.setValue(Settings.cookieHeaderValue, forHTTPHeaderField: "Cookie")
        request.setValue(Settings.acceptHeaderValue, forHTTPHeaderField: "Accept")

        let cancelExpectation = expectation(description: "All tasks by creator with id \(creatorId)")
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                do {
                    guard
                        let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                        let tasks = json["data"] as? [[String: Any]]
                    else {
                        fatalError()
                    }
                    var chosenTaskStatus: String? = nil
                    for task in tasks {
                        if task["id"] as? String == taskId {
                            let taskInfo = task["info"] as? [String: Any]
                            let taskStatuses = taskInfo?["statuses"] as? [String: Any]
                            chosenTaskStatus = taskStatuses?["main"] as? String
                            break
                        }
                    }
                    XCTAssertEqual(chosenTaskStatus, "IN_WORK")
                } catch{
                    fatalError()
                }
                cancelExpectation.fulfill()
            } else if let error = error {
                fatalError(error.localizedDescription)
            }
        }

        task.resume()
        waitForExpectations(timeout: 1, handler: nil)
    }
}
