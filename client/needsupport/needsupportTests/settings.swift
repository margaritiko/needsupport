//
//  settings.swift
//  needsupportTests
//
//  Created by Maria Manakhova on 20.03.2022.
//

import Foundation

struct Settings {
    @available(*, unavailable) private init() {}
    static let scheme: String = "https"
    static let host: String = "staging.crm.needsupport.ru"
    static let cookieHeaderValue: String = "session=.eJwljjkOAjEMAP-SmsKO44vPoDhxBO0uWyH-TiTa0Yw0n_JYR57Pcn8fV97K4zXLvRBNm6Gi6qNTJyZN1andIMLbJgCLseLAiBCV2n0ZGTUZ0bAF1NbqstGdCbB52oaVO80cSW5sw1FrQEdxo7nqYAFMhR1MKHvkOvP43zAwiGy9fH_J6y9R.YjbzVg.2sAd_B1VgcqRlNXsqZpmNYw7jnA"
    static let acceptHeaderValue: String = "application/json"
}
